#pragma once

#include "ofMain.h"
#include "ofComponentStack.h"
#include "WavPlayer_2.h"
#include "WavPlayer.h"
class ofApp : public ofComponentStack {

	public:
		ofApp();
		~ofApp();
private:
	WavPlayer_2 w;
};
