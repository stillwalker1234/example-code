#include "WavPlayer_2.h"

SegCluster* WavPlayer_2::addCluster(string & name, ofBuffer & buf)
{
	clusts.push_back(
		SegCluster(name, [&]()->float {
				return viewPort.width / ( sb.getDurationMS() * 0.1f ); 
			}, [&]()->float {
				return (float(playPosition) / float(sb.getNumFrames())) * sb.getDurationMS() * 0.1f;
			}, [&](int time)->void {
				auto newPos = int((float(time) / (sb.getDurationMS() * 0.1f)) * float(sb.getNumFrames()));
				playPosition = newPos;
			}, buf));
	SegmentsView();
	return &clusts.back();
}

void WavPlayer_2::changeView(ofRectangle _view)
{
	auto view = ofRectangle(_view.x - xOffSet, _view.y, _view.width + widthOffSet, _view.height);
	IComponent::changeView(view);
	SegmentsView();
}

float WavPlayer_2::getTimeToPixelFactor() {
	return viewPort.width / sb.getDurationMS();
}

void WavPlayer_2::setup(ofRectangle view)
{
	setupGui(view);

	SegmentsView();
}

void WavPlayer_2::SegmentsView() {
	for (int i = 0, h = 0; i < clusts.size(); ++i, h += clusts[i - 1].getPerferedHeight()) {
		auto v = ofRectangle( viewPort.x, ((20 * (i + 1)) + h) + (viewPort.y+ viewPort.height)
							, viewPort.width
							, clusts[i].getPerferedHeight() );
		clusts[i].setup(v);
	}
}

inline void WavPlayer_2::zoom(bool zoomOut) {
	auto direction = zoomOut ? (-1.0f*zoomStep) : zoomStep;
	auto pos = float(playPosition) / float(sb.getNumFrames());
	auto newXOffSet = pos * direction;

	if (false) {
		widthOffSet = 0.0f;
		xOffSet = 0.0f;
	}
	else {
		xOffSet = newXOffSet;
		widthOffSet = direction;
	}
	changeView(ofRectangle(viewPort.x, viewPort.y, viewPort.width, viewPort.height));
}

void WavPlayer_2::pan(bool pan)
{
	auto direction = !pan ? (-1.0f*zoomStep) : zoomStep;
	xOffSet = direction;
	changeView(viewPort);
}

void WavPlayer_2::update()
{
	updateWaveBuffer(0, 0, viewPort.width, viewPort.height);

	for (int i = 0; i < clusts.size(); i++)
	{
		clusts[i].update();
	}
}

void WavPlayer_2::draw()
{
	ofSetColor(20, 20, 20);
	//ofRect(viewPort);
	if (sb.getNumFrames() == 0) return;

	// waveObject->drawWaveBuffer(viewPort.x, viewPort.y);
	ofSetColor(255);

	waveForm.draw(viewPort.x, viewPort.y);

	ofSetColor(100);

	ofRect(
		( ( (float)playPosition / (float)( sb.getNumFrames() ) ) * viewPort.width ) + viewPort.x,
		viewPort.y,
		3,
		viewPort.height);

	for (int i = 0; i < clusts.size(); i++)
	{
		clusts[i].draw();
	}
}

void WavPlayer_2::exit()
{
}

void WavPlayer_2::keyPressed(int key)
{
	switch (key)
	{
	case ' ':
		playing = !playing;
		break;
	case 'q':
		playPosition = 0;
		break;
	case 'r': // zoom out
		zoom(true);
		break;
	case 't': // zoom in
		zoom(false);
		break;
	case 'w': // pan left
		pan(true);
		break;
	case 'e': // pan right
		pan(false);
		break;
	default:
		break;
	}
	if (key == ' ') {
	}
	else if (key == 'w') {
	}
}

void WavPlayer_2::keyReleased(int key)
{
}

void WavPlayer_2::mouseMoved(int x, int y)
{
}

void WavPlayer_2::mouseDragged(int x, int y, int button)
{
}

void WavPlayer_2::mousePressed(int x, int y, int button)
{
	if (button == 0 && viewPort.inside(ofPoint(x, y))) {
		playPosition = ((x - viewPort.x) / viewPort.width) * sb.getNumFrames();
	}
}

void WavPlayer_2::mouseReleased(int x, int y, int button)
{
}

void WavPlayer_2::dragEvent(ofDragInfo dragInfo)
{
	if (viewPort.inside(dragInfo.position)) {
		for (int i = 0; i < dragInfo.files.size(); i++)	{

			auto dp = ofToDataPath(dragInfo.files[i]);
			ofFile fp(dp);
			
			if (fp.getExtension() == "wav")
				loadWavFromPath(fp);
			else if (fp.getExtension() == "seg")
				loadSegFromFile(fp);
			
			fp.close();
		}
	}
}

void WavPlayer_2::loadSegFromFile(ofFile & fp) {
	auto buf = fp.readToBuffer();
	SegCluster* current = addCluster(fp.getBaseName(), buf);
}

void WavPlayer_2::loadWavFromPath(ofFile & fp) {
	playing = false;
	playPosition = 0;
	
	ofLog(ofLogLevel::OF_LOG_NOTICE, "file loaded... converting to ofSoundBuffer");

	auto buf = fp.readToBuffer();
	auto s = buf.size();
	const char* d = buf.getData();
	short numChannels = (short)(d[22]);
	int samplerate = *(int*)(d + 24);
	int size = (*(int*)(d + 40)) / sizeof(short);
	sb = ofSoundBuffer((short*)(d + 44), (size_t)size, (size_t)numChannels, samplerate);
	sb.setNumChannels(numChannels);

	if (samplerate != StaticObjs::soundStream.getSampleRate()) {
		ofLog(ofLogLevel::OF_LOG_NOTICE, "resampling...");
		sb.resample((float)samplerate / (float)StaticObjs::soundStream.getSampleRate());
		sb.setSampleRate(StaticObjs::soundStream.getSampleRate());
	}

	ofLog(ofLogLevel::OF_LOG_NOTICE, "done!");
	waveformNeedUpdate = true;
}
void WavPlayer_2::gotMessage(ofMessage msg)
{
}

void WavPlayer_2::audioOut(float * output, int bufferSize, int nChannels)
{
	if (sb.size() == 0) return;

	if(playing == true) {
		for (int i = 0; i < bufferSize; i++) {
			for (int c = 0; c < nChannels; c++)
			{
				output[i*nChannels + c] = sb.getSample(playPosition, c);;
			}
			playPosition++;			
		}
	}
}

float WavPlayer_2::getPerferedHeight()
{
	return 80.0f;
}

void WavPlayer_2::updateWaveBuffer(unsigned int startSmpl/*=0*/, int length/*=0*/, float width /*= 0*/, float height /*= 0*/) {
	if (width == waveFormWidth && height == waveFormHeight && !waveformNeedUpdate) return;

	waveFormHeight = height;
	waveFormWidth = width;
	waveForm.allocate(width, height);

	waveForm.begin();
	ofSetColor(225);
	ofRect(0, 0, width, height);

	if (sb.getNumFrames() == 0) {
		waveForm.end();
		return;
	}
	//isBlocked = true;
	ofSetColor(150);

	int channels = sb.getNumChannels();

	// calculate and constraint the start and end point of the buffer to draw...
	if (length == 0) length = sb.getNumFrames();
	if (startSmpl*channels >= sb.getBuffer().size()) startSmpl = sb.getBuffer().size() - channels;
	if ((startSmpl + length)*channels>sb.getBuffer().size()) length = (sb.getNumFrames()) - startSmpl;

	float per = length / width;

	for (int i = 0; i < width; ++i) {
		float h = 0.0f;
		h = abs((sb.getBuffer()[int((i*per) + startSmpl)*channels] * height));
		ofRect(i, height / 2 - h, 1, h * 2);
	}
	// isBlocked = false;
	waveForm.end();
	waveformNeedUpdate = false;
}