#pragma once
#include "ofMain.h"
#include "IComponent.h"
#include "StaticObjs.h"
#include "SegCluster.h"

class WavPlayer_2 : public IComponent {
	// Inherited via IComponent
public:
	virtual void setup(ofRectangle view) override;
	virtual void update() override;
	virtual void draw() override;
	virtual void exit() override;
	virtual void keyPressed(int key) override;
	virtual void keyReleased(int key) override;
	virtual void mouseMoved(int x, int y) override;
	virtual void mouseDragged(int x, int y, int button) override;
	virtual void mousePressed(int x, int y, int button) override;
	virtual void mouseReleased(int x, int y, int button) override;
	virtual void dragEvent(ofDragInfo dragInfo) override;
	void loadSegFromFile(ofFile & fp);
	void loadWavFromPath(ofFile & fp);
	virtual void gotMessage(ofMessage msg) override;
	virtual void audioOut(float * output, int bufferSize, int nChannels) override;
	virtual float getPerferedHeight() override;
	void updateWaveBuffer(unsigned int startSmpl, int length, float width, float height);
	SegCluster* addCluster(string & name, ofBuffer & buffer);
	void changeView(ofRectangle _view) override;
	float getTimeToPixelFactor();
private:
	void SegmentsView();
	ofFbo waveForm;
	float waveFormWidth, waveFormHeight, xOffSet = 0, widthOffSet = 0, zoomStep = 80;
	ofSoundBuffer sb;
	int playPosition = 0;
	bool playing, waveformNeedUpdate = false;
	void zoom(bool zoomOut);
	void pan(bool pan);
	vector<SegCluster> clusts;
};
