#include "ofMain.h"
#include "ofxGui.h"

#pragma once

class IComponent
{
public:
	virtual ~IComponent(){}
	virtual void setup(ofRectangle view) = 0;	
	virtual void update() = 0;
	virtual void draw() = 0;
	virtual void exit() = 0;
	virtual void keyPressed(int key) = 0;
	virtual void keyReleased(int key) = 0;
	virtual void mouseMoved(int x, int y) = 0;
	virtual void mouseDragged(int x, int y, int button) = 0;
	virtual void mousePressed(int x, int y, int button) = 0;
	virtual void mouseReleased(int x, int y, int button) = 0;
	virtual void dragEvent(ofDragInfo dragInfo) = 0;
	virtual void gotMessage(ofMessage msg) = 0;
	virtual void audioOut(float * output, int bufferSize, int nChannels) = 0;
	virtual float getPerferedHeight() = 0;
	virtual void changeView(ofRectangle _view) {
		viewPort = _view;
		guiPos = ofPoint(viewPort.x, viewPort.y);
		//viewPort.setX((viewPort.x * 2) + guiWidth);
		//viewPort.setWidth(viewPort.width - guiWidth);
	}
protected:
	void setupGui(ofRectangle _viewPort) {			
		gui.setup();
		gui.setDefaultWidth((int)guiWidth);
		changeView(_viewPort);
		gui.setPosition(guiPos);
	}
	ofxPanel gui;
	float guiWidth = 170;
	ofPoint guiPos;
	ofRectangle viewPort;
};