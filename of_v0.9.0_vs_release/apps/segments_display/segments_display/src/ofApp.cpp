#include "ofApp.h"

ofApp::ofApp()
{
	ofSetFrameRate(30);
	addComponent(&w);

	StaticObjs::soundStream.printDeviceList();
	StaticObjs::soundStream.setDeviceID(1);
	StaticObjs::soundStream.setup(this, NUM_CHANNELS, NUM_CHANNELS, SAMPLE_RATE, STREAM_BUFFER_SIZE, 4);
}

ofApp::~ofApp()
{
}
