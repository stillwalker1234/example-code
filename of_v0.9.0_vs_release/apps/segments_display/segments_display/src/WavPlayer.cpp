#include "WavPlayer.h"
#include "StaticObjs.h"

//--------------------------------------------------------------

WavPlayer::WavPlayer() : isPlaying(false) {}

WavPlayer::WavPlayer(function<void(vector<float> data)> target) : target(target)
{
	online = true;
}

WavPlayer::~WavPlayer(){}

void WavPlayer::setup(ofRectangle view) {
	setupGui(view);
	waveStart = 0;
	waveLength = 0;
	meshDetail = 0;

	isRecording = false;
	isPlaying = false;
	waveObject = new ofxWaveHandler(WAVEBUFFER_MINSEC, viewPort.width, viewPort.height);

}

//--------------------------------------------------------------
void WavPlayer::update() {
	waveObject->updateWaveBuffer(waveStart, waveLength, viewPort.width, viewPort.height);
}

//--------------------------------------------------------------
void WavPlayer::draw(){
	ofSetColor(20, 20, 20);
	if (waveObject->getBufferLengthSmpls() > 0) {

		waveObject->drawWaveBuffer(viewPort.x, viewPort.y);

		ofRect(
			(((float)playPosition / (float)(waveObject->getBufferLengthSmpls()))*viewPort.width) + viewPort.x,
			viewPort.y, 
			3, 
			viewPort.height);
	}
	else {
		ofRect(viewPort);
	}
}	

//--------------------------------------------------------------
void WavPlayer::audioOut(float* output, int bufferSize, int nChannels){
	if (isPlaying && online) {
		for (int i = 0; i < bufferSize; i++){
			for (int c = 0; c < nChannels; c++)
			{
				output[i*nChannels+c] = waveObject->getSample(playPosition, c);
			}
			playPosition++;
			waveCurrent = waveLength;

			if (waveCurrent == 0 || waveCurrent > waveObject->getBufferLengthSmpls()) {
				waveCurrent = waveObject->getBufferLengthSmpls();				
			}

			if (playPosition > waveCurrent + waveStart) {
				isPlaying = false;
				playPosition = waveStart;
				auto b = false;
				ofNotifyEvent(fileFinEvent, b);
				for (int i = 0; i < bufferSize*nChannels; i++)
				{
					output[i] = 0.0f;
				}
				break;
			}
		}
	}
}


void WavPlayer::advance(int num)
{
	vector<float> LR(num << 2, 0.0f);
	auto bufferSize = num << 1;
	auto nChannels = 2;
	if (isPlaying) {
		waveCurrent = waveObject->getBufferLengthSmpls();

		if (waveCurrent == 0) {
			isPlaying = false;
			return;
		}

		for (int i = 0; i < bufferSize; i++) {
			for (int c = 0; c < nChannels; c++)
			{
				LR[i*nChannels + c] = waveObject->getSample(playPosition, c);
			}
			playPosition++;

			if (playPosition > waveCurrent) {
				ofLogNotice(ofToString(waveCurrent));
				ofLogNotice(ofToString(playPosition));
				isPlaying = false;
				playPosition = waveStart;
				auto b = false;
				ofNotifyEvent(fileFinEvent, b);
				return;
			}
		}

		target(LR);
	}
}

float WavPlayer::getPerferedHeight()
{
	return 80.0f;
}

void WavPlayer::loadAndPlay(string path)
{
	waveObject->clearBuffer();
	waveObject->loadBuffer(path);
	playPosition = 0;
	isPlaying = isStart = true;
}

//--------------------------------------------------------------
void WavPlayer::keyPressed(int key){
	
	if (key == ' ') {			
		isPlaying = !isPlaying;
		isStart = true;
	}
	else if (key == 'w') {
		playPosition = 0;

	}
}

//--------------------------------------------------------------
void WavPlayer::keyReleased(int key){

}

//--------------------------------------------------------------
void WavPlayer::mouseMoved(int x, int y){

}

//--------------------------------------------------------------
void WavPlayer::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void WavPlayer::mousePressed(int x, int y, int button){
	if (button == 0 && viewPort.inside(ofPoint(x, y))) {
		playPosition = ((x - viewPort.x) / viewPort.width) * waveObject->getBufferLengthSmpls();
	}
}

//--------------------------------------------------------------
void WavPlayer::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void WavPlayer::windowResized(int w, int h){

}

//--------------------------------------------------------------
void WavPlayer::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void WavPlayer::dragEvent(ofDragInfo dragInfo){
	if (viewPort.inside(dragInfo.position)) {
		isPlaying = false;
		playPosition = 0;
		waveObject->loadBuffer(dragInfo.files[0]);
	}
}

//--------------------------------------------------------------
void WavPlayer::exit(){
	delete waveObject;
}
