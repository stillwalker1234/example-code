#pragma once

#include "ofMain.h"
#include "IComponent.h"
#include "StaticObjs.h"

class SegCluster : public IComponent {
public:
	SegCluster(string & name, function<float()> toPixel, function<float()> getTime, function<void(int)> setTime, ofBuffer & buf);

	// Inherited via IComponent
	virtual void setup(ofRectangle view) override;

	virtual void update() override;


	virtual void draw() override;

	virtual void exit() override;

	virtual void keyPressed(int key) override;

	virtual void keyReleased(int key) override;

	virtual void mouseMoved(int x, int y) override;

	virtual void mouseDragged(int x, int y, int button) override;

	virtual void mousePressed(int x, int y, int button) override;

	virtual void mouseReleased(int x, int y, int button) override;

	virtual void dragEvent(ofDragInfo dragInfo) override;

	virtual void gotMessage(ofMessage msg) override;

	virtual void audioOut(float * output, int bufferSize, int nChannels) override;

	virtual float getPerferedHeight() override;
private:
	class Segment {
	public:
		Segment(float start, float lenght, ofColor c, string name);
		float getStart();
		float getHeight();
		float getWidth();
		string getName();
		ofColor getColor();
	private:
		float start, lenght;
		ofColor c;
		string name;
	};
	Segment * findSegment(int idx, int target);
	string name;
	string diplayText;
	float lastSegmentTime;
	function<float()> toPixel;
	function<float()> getTime;
	function<void(int)> setTime;
	vector<Segment> segments;


};
