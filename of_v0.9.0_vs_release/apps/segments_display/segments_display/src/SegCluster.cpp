#include "SegCluster.h"


SegCluster::SegCluster(string & name, function<float()> toPixel, function<float()> getTime, function<void(int)> setTime, ofBuffer & buf) : 
	name(name), 
	toPixel(toPixel),
	getTime(getTime),
	setTime(setTime)
{
	auto r = ofSplitString(buf.getFirstLine(), " ", false, true);
	int colorCycle = 0;
	lastSegmentTime = 0.0f;
	while (!buf.isLastLine()) {
		if (r[0][0] == ';') {
			colorCycle = (colorCycle + 1) % 8;
		}
		else if (r.size() >= 8) {
			auto start = ofFromString<float>(r[2]);
			auto lenght = ofFromString<float>(r[3]);
			if ((start + lenght) > lastSegmentTime) lastSegmentTime = (start + lenght);

			segments.push_back( Segment( start
									   , lenght
									   , ofColor::fromHsb( (float(colorCycle) / 7.0f)*255.0f
										 				 , 255.0f
										 				 , 127.0f
														 , 255.0f )
									   , r[7] ) ); // name 
		}
		r = ofSplitString(buf.getNextLine(), " ", false, true);
	}

	sort( segments.begin()
		, segments.end()
		, [](Segment l, Segment r)->bool { return l.getStart() < r.getStart(); } );
}

void SegCluster::setup(ofRectangle view)
{
	setupGui(view);
}

void SegCluster::update()
{
	auto time = getTime();
	auto idx = min(int(segments.size()-2)
			, int((float(time) / lastSegmentTime) * segments.size()));

	auto Seg = findSegment(idx, time);
	
	diplayText = name + " : ";
	if (NULL != Seg) {
		diplayText += Seg->getName();
	}
}

// div & coq
SegCluster::Segment* SegCluster::findSegment(int idx, int target) {

		if (segments[idx].getStart() > target) {
			if (idx == 0)
				return NULL;
			else 
				return findSegment(idx-1, target);
		}
		else if ((segments[idx].getStart() + segments[idx].getWidth()) >= target)
			return &segments[idx];
		else if (idx == (segments.size() - 1) || (segments[idx+1].getStart()>target))
			return NULL;
		else
			return findSegment(idx + 1, target);
}

void SegCluster::draw()
{
	ofDrawBitmapString(diplayText, 15, viewPort.y);

	for (int i = 0; i < segments.size(); i++)
	{
		ofSetColor(segments[i].getColor());
		auto tmp = toPixel();
		ofRect( viewPort.x + ( segments[i].getStart() * toPixel() )
			  , viewPort.y
			  , ( segments[i].getWidth() * toPixel() )
			  , viewPort.height );
	}
}

void SegCluster::exit()
{
}

void SegCluster::keyPressed(int key)
{
}

void SegCluster::keyReleased(int key)
{
}

void SegCluster::mouseMoved(int x, int y)
{
}

void SegCluster::mouseDragged(int x, int y, int button)
{
}

void SegCluster::mousePressed(int x, int y, int button)
{
}

void SegCluster::mouseReleased(int x, int y, int button)
{
}

void SegCluster::dragEvent(ofDragInfo dragInfo)
{
}

void SegCluster::gotMessage(ofMessage msg)
{
}

void SegCluster::audioOut(float * output, int bufferSize, int nChannels)
{
}

float SegCluster::getPerferedHeight()
{
	return 35.0f;
}

inline SegCluster::Segment::Segment(float start, float lenght, ofColor c, string name) : start(start), lenght(lenght), c(c), name(name) {

}

inline float SegCluster::Segment::getStart() {
	return start;
}

inline float SegCluster::Segment::getHeight() {
	return 35;
}

inline float SegCluster::Segment::getWidth() {
	return lenght;
}

string SegCluster::Segment::getName()
{
	return name;
}

inline ofColor SegCluster::Segment::getColor() {
	return c;
}
