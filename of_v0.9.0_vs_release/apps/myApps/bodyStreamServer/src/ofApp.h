#pragma once

#define KINECT_COUNT 3
#include "ofMain.h"
#include "ofxOsc.h"
#include "ofBodyWithForce.h"
#include "kinect.h"
#include "ball.h"

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
private:
	vector<ofEasyCam> cams;
	vector<ofMesh> mesh;

	bool doDraw = true;
	ofxOscSender sender;
	ofxOscReceiver receiver;
	bool hasBodyTrack = false;
	vector<BodyWithForce> bodies;
	vector<LowPass3d> filters;

	int flipOrient = 0;
	Ball leftHandBall;
	Ball rightHandBall;
	vector<int> j2flp = { 408, 509, 610, 711, 1216, 1317, 1418, 1519, 2123, 2224 };
	float getCost();
	BodyWithForce body;
	vector<ofVec4f> floorPlanes;
	vector<bool> showBody;



	auto of2cam(ofVec3f x) {
		CameraSpacePoint ret;
		ret.X = x.x;
		ret.Y = x.y;
		ret.Z = x.z;
		return ret;
	}

	void setCirkleCenter(ofVec3f& cog) {
		cirkleCenter = ks2gls(floorTrickBlock::pointOnFloor(cog, floorPlanes[0]));
	}

	void setCirkleRadius(ofVec3f& cog) {
		cirkleRadius = (cirkleCenter - ks2gls(floorTrickBlock::pointOnFloor(cog, floorPlanes[0]))).length();
	}

	// kinect space -> openGL space
	ofVec3f ks2gls(ofVec3f gl_point) {
		gl_point.z = 10. - gl_point.z;
		gl_point *= 10.;
		return gl_point;
	}

	// openGL space -> kinect space
	ofVec3f gls2ks(ofVec3f gl_point) {
		gl_point /= 10.;
		gl_point.z = 10. - gl_point.z;
		return gl_point;
	}
	ofVec4f ks2gls(ofVec4f gl_point) {
		gl_point.z = 10. - gl_point.z;
		//gl_point.w *= 10.;
		return gl_point;
	}

	ofVec3f cirkleCenter = ofVec3f(0.805457, -7.46776, 74.0713);
	float cirkleRadius = 24.1146;

	void sendMes(floorTrickBlock::forceData hand, floorTrickBlock::forceData foot, ofVec3f dir, Ball ball, ofVec3f cog, string addrs);
};
