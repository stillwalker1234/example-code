#pragma once

#include "ofMain.h"

class Ball
{
public:
	Ball(float drag_factor = .2, float edge = 200., float edge_factor = .0) : drag_factor(drag_factor), edge(edge), edge_factor(edge_factor) {}
	~Ball() {}

	void setup(ofVec3f* c) {
		center = c;
		edge = (*center).length()*30.;
	}

	void update(ofVec3f new_hand_pos) {
		if (new_hand_pos.x > -1000. && new_hand_pos.x < 1000.) {
			auto drag = (new_hand_pos - (c_pos+direction)) * drag_factor;
			auto hand_vel = new_hand_pos - old_hand_pos;
			old_hand_pos = new_hand_pos;

			if (isConnected) {
				auto new_direction = (direction + drag + hand_vel)*.8;
				if ((new_direction - direction).length() * (c_pos - new_hand_pos).length() > 5.)
					isConnected = false;
				direction = (direction + drag + hand_vel)*.8;
				c_pos = c_pos + direction;
			}
			else {
				auto new_direction = direction*.8;
				if ((c_pos + new_direction).length() > maxDistance || new_direction.length() < .1 ) {
					isConnected = true;
					c_pos = new_hand_pos;
				}
				else
					c_pos = c_pos + direction;
			}
		
		}
	}

	const ofVec3f getPosition() {
		return c_pos;
	}

private:
	float sigmoid(float _in, float shift) {
		return 1. / (1. + exp(-((_in-shift)*10.)));
	}
	bool isConnected = true;
	float strechLimit = 40.;
	float maxDistance = 100.;
	ofVec3f* center;
	ofVec3f direction, c_pos, old_hand_pos;
	float drag_factor, edge, edge_factor;
};
