#pragma once

#include "Butterworth.h"

class LowPass3d
{
public:
	LowPass3d() {
		_filter.setup(3, 32., 6.);
	}
	~LowPass3d() {}

	void process(float* x, float* y, float* z) {
		float* _in[] = { x, y, z };
		_filter.process<float>(1, _in);
	}
private:
	Dsp::SimpleFilter<Dsp::Butterworth::LowPass<3>, 3> _filter;
};