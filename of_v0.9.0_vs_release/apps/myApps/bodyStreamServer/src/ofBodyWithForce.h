#pragma once

#include "ofMain.h"
#include "ofxKinect2.h"
#include "Butterworth.h"

const float joint_weights[] = {
	8.88, // 0
	12.7, // 1
	2.46,  // 2
	5.74,  // 3
	5.58,  // 4
	3.36,  // 5
	1.11,  // 6
	0.4,  // 7
	5.58,  // 8
	3.36,  // 9
	1.11,  // 10
	0.4,  // 11
	8.51,  // 12
	9.3,  // 13
	2.4,  // 14
	0.2,  // 15
	8.51,  // 16
	9.3,  // 17
	2.4,  // 18
	0.2,  // 19
	9.3,  // 20
	0.3,  // 21
	0.1,  // 22
	0.3,  // 22
	0.1   // 23 
};


class gravityBlock
{
public:
	gravityBlock() : lambda(0.7) {};
	~gravityBlock() {};

	function<void(vector<Joint>&, int)> kernel = [&](vector<Joint>& body, int i) {
		weight_sum += joint_weights[i];
		sum += joint_weights[i] * cameraSpacePoint2ofVec3f(body[i].Position);
	};

	auto pop() {
		auto ret = sum / weight_sum;
		sum = ofVec3f().zero();
		weight_sum = 0.0;
		auto c_force = (ret - positionOld);
		if (c_force.length() > 0.0) {

			if (c_force.length() > velocityForce.length()) {
				velocityForce = ((1. - lambda) * velocityForce) + (lambda * c_force);
			}
			else {
				velocityForce = (lambda * velocityForce) + ((1. - lambda) * c_force);
			}
			positionOld = ret;
		}
		return make_tuple(ret, velocityForce*powf(velocityForce.lengthSquared() + 1., 3.));
	}

private:
	float lambda;
	ofVec3f cameraSpacePoint2ofVec3f(CameraSpacePoint p) {
		return ofVec3f(p.X, p.Y, p.Z);
	}
	ofVec3f sum = ofVec3f().zero();
	ofVec3f positionOld = ofVec3f().zero();
	ofVec3f velocityForce;
	float weight_sum = 0.0;
};

class floorTrickBlock
{
public:
	floorTrickBlock() : isTouch(vector<forceData>(JointType_Count)) {};

	struct forceData
	{
		ofVec3f position;
		float force;
		float speed;
		TrackingState _state;
		bool touchState;
		JointType type;
		float angle;
	};

	function<void(vector<Joint>&, int)> kernel = [&](vector<Joint>& body, int i) {
		auto& f = isTouch[i];
		f.touchState = ((getDistanceFromFloor(body[i].Position) < tol)
			&& body[i].TrackingState == TrackingState_Tracked) ? true : false;
		
		f._state = body[i].TrackingState;
		auto new_pos = cam2Of(body[i].Position);
		f.speed = (f.position - new_pos).lengthSquared();
		f.position = new_pos; 
		f.type = body[i].JointType;
	};

	void setFloorPointer(ofVec4f* fp) {
		floorPlane = fp;
	}

	float getDistanceFromFloor(CameraSpacePoint point)
	{
		return ((floorPlane->x*point.X) + (floorPlane->y*point.Y) + (floorPlane->z*point.Z) + (floorPlane->w))
			/ sqrt(powf(floorPlane->x, 2.) + powf(floorPlane->y, 2.) + powf(floorPlane->z, 2.));
	}

	static ofVec3f pointOnFloor(ofVec3f& inPoint, ofVec4f& floorPlane) {
		auto n = ofVec3f(floorPlane);
		auto distance = n.dot(inPoint)+floorPlane.w;
		return inPoint - (distance*n);
	}

	ofVec3f getPlane() {
		return ofVec3f(*floorPlane);
	}

	auto pop() {
		return isTouch;
	}

private:
	ofVec3f cam2Of(CameraSpacePoint p) { return ofVec3f(p.X, p.Y, p.Z); }
	vector<forceData> isTouch;
	float tol = 3e-1;
	ofVec4f* floorPlane;
};

class forceOnGroundBlock : gravityBlock, public floorTrickBlock
{
public:
	forceOnGroundBlock() {};
	~forceOnGroundBlock() {};

	auto getKernel(vector<Joint>& current_body) {
		untrackedRatio = 0.;
		auto kernel = [&](int j) {
			gravityBlock::kernel(current_body, j);
			floorTrickBlock::kernel(current_body, j);

			if (
				(j==0 || j==1 || j==2 || j==3 || j==20
					|| j==4 || j==5 || j==6
					|| j==8 || j==9 || j==10
					|| j==12 || j==13 || j==14
					|| j==16 || j==17 || j==18)
				&& current_body[j].TrackingState != TrackingState_Tracked)
				untrackedRatio += 1.;
		};

		return kernel;
	};

	auto forceOnGroundBlock::pop() {
		ofVec3f cog, vel_force;

		tie(cog, vel_force) = gravityBlock::pop();
		auto& isTouch = floorTrickBlock::pop();
		auto angles_sum = 0.;
		auto gravity_force = -.05*floorTrickBlock::getPlane();
		for (int i = 0; i<isTouch.size(); i++) {
			if (isTouch[i].touchState) {
				isTouch[i].force = powf((90.0 - (-1.*(gravity_force + vel_force)).angle(cog - isTouch[i].position)) / 90.0, 14.0);
				angles_sum += isTouch[i].force;
			}
		}
		for (int i = 0; i<isTouch.size(); i++) {
			if (isTouch[i].touchState) {
				isTouch[i].force /= angles_sum;
				isTouch[i].force *= (gravity_force + vel_force).length()*10.;
			}
		}
		return make_tuple(cog, isTouch, gravity_force, vel_force, gravity_force + vel_force);
	}

protected:
	float untrackedRatio;
};

class BodyWithForce : public forceOnGroundBlock {
public:
	BodyWithForce() : hasData(false), sumSpeed(1e-5), touchJoints(vector<floorTrickBlock::forceData>(JointType_Count)) 
	{
		_filter.setup(9, 600, 2.);
	}
	vector<LowPass3d> filter;
	void update(vector<Joint>& body, vector<JointOrientation>& body_o);
	void update(vector<Joint>& body);
	bool hasData;
	vector<floorTrickBlock::forceData> touchJoints;
	float orientation, rotation, angle, sumSpeed;
	ofVec3f cog, g_force, v_force, sum_force;

	void moveBody(ofVec3f ref, ofVec3f axis) {
		auto translation = ref - cog;

		for (auto& item : touchJoints) {
			item.position += translation;
			item.position = item.position.rotate(rotation, ref, axis);
		}
	}

	void calcAvgSpeed() {
		float tempSpeed = 0.;
		for (auto& j : touchJoints) {
			if(j.type == JointType_SpineBase)
				tempSpeed += j.speed; 
		}

		tempSpeed *= 4.;
		// sumSpeed = ofClamp(sumSpeed, 1e-5, 1.);

		tempSpeed = ofMap(tempSpeed, 0.00001, .08, 0., 1., true);
		auto temp2Speed = (4e-1 * tempSpeed) + sumSpeed;
		
		if (temp2Speed < 1.)
			sumSpeed = temp2Speed;

		if(sumSpeed > 0.)
			sumSpeed -= 7e-3;
		
	}

private:
	Dsp::SimpleFilter<Dsp::Butterworth::LowPass<9>, 1> _filter;
};