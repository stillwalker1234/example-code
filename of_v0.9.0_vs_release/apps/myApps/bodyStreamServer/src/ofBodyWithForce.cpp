#include "ofBodyWithForce.h"

void BodyWithForce::update(vector<Joint>& body, vector<JointOrientation>& body_o)
{
		BodyWithForce::update(body);
		
		for (int j = 0; j < body.size(); j++) {
			auto quat = ofQuaternion(body_o[j].Orientation.x, body_o[j].Orientation.y, body_o[j].Orientation.z, body_o[j].Orientation.w);
			float angle = 0.; auto dummy = floorTrickBlock::getPlane();
			quat.getRotate(angle, dummy);

			touchJoints[j].angle = 1. - ofClamp(abs(angle - 180.) / 50., 0., 1.);
		}

		auto c2l = (touchJoints[4].position - touchJoints[20].position).getNormalized();
		auto c2r = (touchJoints[8].position - touchJoints[20].position).getNormalized();
		auto c2m = (touchJoints[1].position - touchJoints[20].position).getNormalized();

		orientation = ofMap(.1 / ((abs(c2l.dot(c2m)) + abs(c2r.dot(c2m))) / 2.), 0.2, 1., 0., 1., 1);

		angle = (touchJoints[JointType_SpineBase].angle + touchJoints[JointType_AnkleLeft].angle + touchJoints[JointType_AnkleRight].angle) / 3.;
		angle = ofMap(angle, .3, 1., 0., 1., 1);
		/*

		auto hl = (touchJoints[JointType_SpineBase].position - touchJoints[JointType_HipLeft].position).getNormalized();
		auto hr = (touchJoints[JointType_SpineBase].position - touchJoints[JointType_HipRight].position).getNormalized();

		angle = hl.dot(hr);
		angle = .1 / abs(
			touchJoints[JointType_HipLeft].position.getNormalized().dot(
			(touchJoints[JointType_HipRight].position - touchJoints[JointType_HipLeft].position).getNormalized()));
		*/

}

void BodyWithForce::update(vector<Joint>& body)
{
	filter.resize(JointType_Count);

	auto kernel = forceOnGroundBlock::getKernel(body);

	for (int j = 0; j < body.size(); j++) {
		kernel(j);
	}

	tie(cog, touchJoints, g_force, v_force, sum_force) = forceOnGroundBlock::pop();

	for (int j = 0; j < body.size(); j++) {
		if (touchJoints[j].position.x > -1000. && touchJoints[j].position.x < 1000.) // filter will move to unstable state if exited by inf
			filter[j].process(
				&touchJoints[j].position.x,
				&touchJoints[j].position.y,
				&touchJoints[j].position.z);
	}

	hasData = true;
}




