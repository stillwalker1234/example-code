#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
	bodies = vector<BodyWithForce>(KINECT_COUNT + 1);
	cams = vector<ofEasyCam>(KINECT_COUNT+1);
	mesh = vector<ofMesh>(KINECT_COUNT + 1);
	floorPlanes = vector<ofVec4f>(KINECT_COUNT+1);
	showBody = vector<bool>(KINECT_COUNT+1);
	leftHandBall.setup(&cirkleCenter);
	rightHandBall.setup(&cirkleCenter);

	receiver.setup(8000);
	sender.setup("127.0.0.1", 9000);
	ofSetFrameRate(120);
	ofSetBackgroundColor(ofColor::black);
	
	for (int i = 0; i < bodies.size(); i++) bodies[i].setFloorPointer(&floorPlanes[i]);
	bodies[KINECT_COUNT].setFloorPointer(&floorPlanes[0]);
	for (auto& cam : cams) {

		cam.setDistance(100);
		// cam.rotateAround(-20., ofVec3f(1., 0., 0.), ks2gls(cirkleCenter));
	}
	//cams[1].setPosition(ofVec3f(28.9334, 0.445697, 82.649));
	//cams[1].setOrientation(ofVec3f(0.192551, 63.494, 2.99318));

	bodies[1].rotation = 135.;
	bodies[2].rotation = 252.;
}

//--------------------------------------------------------------
void ofApp::update(){
	hasBodyTrack = false;
	filters.resize(JointType_Count);

	while (receiver.hasWaitingMessages()) {
		ofxOscMessage mes;
		receiver.getNextMessage(mes);

		if (mes.getAddress() == "/body") {
			
			auto id = mes.getArgAsInt(0);
			auto planeBuffer = mes.getArgAsBlob(1);
			memcpy(&floorPlanes[id], planeBuffer.getBinaryBuffer(), sizeof(ofVec4f));
			auto bodyBuffer = mes.getArgAsBlob(2);
			auto bodyV = vector<Joint>(JointType_Count);
			memcpy(&bodyV[0], bodyBuffer.getBinaryBuffer(), JointType_Count * sizeof(Joint));

			auto orientBuffer = mes.getArgAsBlob(3);
			auto bodyO = vector<JointOrientation>(JointType_Count);
			memcpy(&bodyO[0], orientBuffer.getBinaryBuffer(), JointType_Count * sizeof(JointOrientation));
				
			bodies[id].update(bodyV, bodyO);

			if (id) {
				bodies[id].moveBody(bodies[0].cog, ofVec3f(floorPlanes[0]));
			}
		}

		else if (mes.getAddress() == "/setCenter") {
			setCirkleCenter(bodies[0].cog);
			ofLogNotice(ofToString(cirkleCenter) + " - center is");
		}

		else if (mes.getAddress() == "/setRadius") {
			setCirkleRadius(bodies[0].cog);
			ofLogNotice(ofToString(cirkleRadius) + " - radius is");
		}

	}

	auto has_suf_data = 0;
	for (size_t k = 0; k < KINECT_COUNT; k++)
	{
		has_suf_data += bodies[k].hasData;
	}

	if (has_suf_data > 1) {
		auto body_j = vector<Joint>(JointType_Count);

		for (int i = 0; i < JointType_Count; i++) {
			ofVec3f p = ofVec3f(0., 0., 0.);
			Joint j_;
			j_.TrackingState = TrackingState_Tracked;
			j_.JointType = (JointType)i;

			auto items = vector<floorTrickBlock::forceData>(KINECT_COUNT);
			for (size_t k = 0; k < KINECT_COUNT; k++)
			{
				items[k] = bodies[k].touchJoints[i];
			}

			auto weights = vector<float>(KINECT_COUNT, 0.);
			float w_sum = 0.;
			for (int j = 0; j < KINECT_COUNT; j++)
			{
				weights[j] = bodies[j].orientation * bodies[j].angle;
				w_sum += weights[j];
			}

			for (int j = 0; j < KINECT_COUNT; j++)
			{
				p += bodies[j].touchJoints[i].position * (weights[j] / w_sum);
			}

			j_.Position.X = p.x; j_.Position.Y = p.y; j_.Position.Z = p.z;
			body_j[i] = j_;
		}

		bodies[KINECT_COUNT].update(body_j);

		// ball
		leftHandBall.update(bodies[KINECT_COUNT].touchJoints[6].position);
		rightHandBall.update(bodies[KINECT_COUNT].touchJoints[10].position);

		// foot force
		auto l_1 = bodies[KINECT_COUNT].touchJoints[14];
		auto l_2 = bodies[KINECT_COUNT].touchJoints[15];
		auto r_1 = bodies[KINECT_COUNT].touchJoints[18];
		auto r_2 = bodies[KINECT_COUNT].touchJoints[19];

		auto l_force = (l_1.force + l_2.force) / 2.;
		auto r_force = (r_1.force + r_2.force) / 2.;

		bodies[KINECT_COUNT].touchJoints[JointType_AnkleLeft].force = l_force;
		bodies[KINECT_COUNT].touchJoints[JointType_AnkleRight].force = r_force;

		// speed
		bodies[KINECT_COUNT].calcAvgSpeed();

		// send data
		sendMes(bodies[KINECT_COUNT].touchJoints[6], bodies[KINECT_COUNT].touchJoints[JointType_AnkleLeft], 
			bodies[KINECT_COUNT].touchJoints[6].position - bodies[KINECT_COUNT].touchJoints[5].position, 
			leftHandBall, 
			bodies[KINECT_COUNT].cog, "/left");
		sendMes(bodies[KINECT_COUNT].touchJoints[10], bodies[KINECT_COUNT].touchJoints[JointType_AnkleRight],
			bodies[KINECT_COUNT].touchJoints[10].position - bodies[KINECT_COUNT].touchJoints[9].position,
			rightHandBall, bodies[KINECT_COUNT].cog, "/right");

		auto mes = ofxOscMessage();
		mes.setAddress("/center");
		auto cog_onfloor = floorTrickBlock::pointOnFloor(bodies[KINECT_COUNT].cog, floorPlanes[0]);
		auto head_onfloor = floorTrickBlock::pointOnFloor(bodies[KINECT_COUNT].touchJoints[JointType_Head].position, floorPlanes[0]);
		mes.addFloatArg((cog_onfloor - bodies[KINECT_COUNT].cog).length());
		mes.addFloatArg(bodies[KINECT_COUNT].sumSpeed);
		mes.addFloatArg(ofMap((cog_onfloor - head_onfloor).lengthSquared(), 0., 2e-1, 0., 1., true));

		sender.sendMessage(mes, false, 1024);

		bodies[KINECT_COUNT].hasData = true;


		for (int i = 0; i < bodies.size(); i++) {
			if (bodies[i].hasData) {
				mesh[i] = ofMesh();
				mesh[i].setMode(OF_PRIMITIVE_LINES);
				mesh[i].usingIndices();
				mesh[i].usingColors();
				for (auto& j : bodies[i].touchJoints) {
					if (i != KINECT_COUNT) {
						mesh[i].addVertex(ks2gls(j.position));
						if (j._state != TrackingState_Tracked) {
							mesh[i].addColor(ofFloatColor(1., 0., 1.));
						}
						else {
							mesh[i].addColor(ofFloatColor(1., 1., 0.));
						}
					}
					else {
						mesh[i].addVertex(ks2gls(j.position));
						mesh[i].addColor(ofFloatColor(0., 1., 1.));
					}
				}


				mesh[i].addIndex(3);
				mesh[i].addIndex(2);
				mesh[i].addIndex(2);
				mesh[i].addIndex(20);

				mesh[i].addIndex(20);
				mesh[i].addIndex(8);

				mesh[i].addIndex(20);
				mesh[i].addIndex(4);

				// left arm
				mesh[i].addIndex(4);
				mesh[i].addIndex(5);
				mesh[i].addIndex(5);
				mesh[i].addIndex(6);
				//mesh[i].addIndex(6);
				//mesh[i].addIndex(7);
				//mesh[i].addIndex(7);
				//mesh[i].addIndex(21);
				//mesh[i].addIndex(6);
				//mesh[i].addIndex(22);

				// right arm
				mesh[i].addIndex(8);
				mesh[i].addIndex(9);
				mesh[i].addIndex(9);
				mesh[i].addIndex(10);
				//mesh[i].addIndex(10);
				//mesh[i].addIndex(11);
				//mesh[i].addIndex(11);
				//mesh[i].addIndex(23);
				//mesh[i].addIndex(10);
				//mesh[i].addIndex(24);

				// center body
				mesh[i].addIndex(20);
				mesh[i].addIndex(1);
				mesh[i].addIndex(1);
				mesh[i].addIndex(0);

				// left leg

				mesh[i].addIndex(0);
				mesh[i].addIndex(12);
				mesh[i].addIndex(12);
				mesh[i].addIndex(13);
				mesh[i].addIndex(13);
				mesh[i].addIndex(14);


				//mesh[i].addIndex(14);
				//mesh[i].addIndex(15);
				// right leg
				mesh[i].addIndex(0);
				mesh[i].addIndex(16);
				mesh[i].addIndex(16);
				mesh[i].addIndex(17);
				mesh[i].addIndex(17);
				mesh[i].addIndex(18);
				//mesh[i].addIndex(18);
				//mesh[i].addIndex(19);
				bodies[i].hasData = false;
			}
		}
	}

}

//--------------------------------------------------------------
void ofApp::draw(){
	
	for (size_t i = 0; i < KINECT_COUNT; i++)
	{
		cams[i].begin();

		if(showBody[i])
			mesh[i].draw();

		ofSetColor(ofColor::red);
		for (int j = 0; j < 10.; j++) {
			auto p1 = floorTrickBlock::pointOnFloor(ofVec3f(5., 0., float(j)), floorPlanes[i]);
			auto p2 = floorTrickBlock::pointOnFloor(ofVec3f(-5., 0., float(j)), floorPlanes[i]);
			auto p3 = ks2gls(p1);
			auto p4 = ks2gls(p2);
			ofDrawLine(p3, p4);
		}
		cams[i].end();
	}

	ofDrawRectangle(ofPoint(10., 20, 0.), bodies[KINECT_COUNT].sumSpeed * 200., 10.);
	ofSetColor(ofColor::green);
	ofDrawRectangle(ofPoint(210., 20, 0.),10., 10.);
	ofDrawBitmapString(ofToString(bodies[1].orientation), ofPoint(10., 40, 0.));
	ofDrawBitmapString(ofToString(bodies[2].orientation), ofPoint(10., 60, 0.));

	ofDrawBitmapString(ofToString(bodies[0].angle), ofPoint(10., 100, 0.));
	ofDrawBitmapString(ofToString(bodies[1].angle), ofPoint(10., 120, 0.));
	ofDrawBitmapString(ofToString(bodies[2].angle), ofPoint(10., 140, 0.));


	cams[KINECT_COUNT].begin();

	if (showBody[KINECT_COUNT]) {

		mesh[KINECT_COUNT].draw();

		auto l_1 = bodies[KINECT_COUNT].touchJoints[14];
		auto l_2 = bodies[KINECT_COUNT].touchJoints[15];
		auto r_1 = bodies[KINECT_COUNT].touchJoints[18];
		auto r_2 = bodies[KINECT_COUNT].touchJoints[19];

		ofSetColor(ofColor::red);
		ofDrawEllipse(ks2gls(l_1.position), 3.* (l_1.force + l_2.force), 3.* (l_1.force + l_2.force));
		ofDrawEllipse(ks2gls(r_1.position), 3.* (r_1.force + r_2.force), 3.* (r_1.force + r_2.force));


		ofSetColor(ofColor::deepPink);
		ofDrawEllipse(ks2gls(rightHandBall.getPosition()), 1., 1.);

		ofSetColor(ofColor::green);

		ofDrawEllipse(ks2gls(leftHandBall.getPosition()), 1., 1.);
	}
	cams[KINECT_COUNT].end();

	/*
	i = angleMin;
	cams[i].begin();
	ofDrawEllipse(ks2gls(bodies[i].touchJoints[JointType_SpineShoulder].position), 5.*bodies[i].orientation, 5.*bodies[i].orientation);

	cams[i].end();
	ofSetColor(ofColor::yellow);

	ofDrawBitmapString(ofToString(getCost()), ofPoint(10., 10, 0.));
	auto c = (cams[0].worldToCamera(bodies[0].cog) - cams[1].worldToCamera(bodies[1].cog)).lengthSquared();
	ofDrawBitmapString(ofToString(cams[1].getGlobalPosition()), ofPoint(10., 20, 0.));
	ofDrawBitmapString(ofToString(cams[1].getOrientationEuler()), ofPoint(10., 40, 0.));
	ofDrawBitmapString(ofToString(angles[0]), ofPoint(10., 60, 0.));
	ofDrawBitmapString(ofToString(bodies[2].touchJoints[JointType_HandLeft].position), ofPoint(10., 80, 0.));
	
	
	cams[0].begin();
	ofSetColor(ofColor::white);
	//mesh[KINECT_COUNT].draw();


	// draw cog
	ofSetColor(ofColor::deepPink);
	ofDrawEllipse(rightHandBall.getPosition(), 1., 1.);

	ofSetColor(ofColor::green);

	ofDrawEllipse(leftHandBall.getPosition(), 1., 1.);

	
	cams[0].end();
	*/
}

void ofApp::sendMes(floorTrickBlock::forceData hand, floorTrickBlock::forceData foot, ofVec3f dir, Ball ball, ofVec3f cog, string addrs) {

	// distance from center and angle

	auto mes = ofxOscMessage();
	mes.setAddress(addrs);


	auto cog_onfloor = floorTrickBlock::pointOnFloor(cog, floorPlanes[0]);
	auto hand_onfloor = floorTrickBlock::pointOnFloor(hand.position, floorPlanes[0]);
	auto p_c = (ks2gls(foot.position) - cirkleCenter) / cirkleRadius;
	
	auto rot = ofVec3f(floorPlanes[0]).getRotated(90., ofVec3f(1., 0., 0.));
	float dir_p = dir.getNormalized().dot(rot) + 1.;

	auto side = dir.x < 0. ? 1. : -1;
	mes.addFloatArg(p_c.x);
	mes.addFloatArg(p_c.z);
	mes.addFloatArg((cog_onfloor - hand_onfloor).length());
	mes.addFloatArg((((dir_p * side) / 2.) * PI) + (PI/2.));
	mes.addFloatArg(foot.force);
	mes.addFloatArg((hand.position - ball.getPosition()).length());
	mes.addFloatArg(foot.speed);
	sender.sendMessage(mes, false, 1024);
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

	switch (key)
	{
	case '1':
		bodies[1].rotation = float((int(bodies[1].rotation) + 3) % 360);
		break;
	case '2':
		bodies[2].rotation = float((int(bodies[2].rotation) + 3) % 360);
		break;
	case '3':
	case '4':
	case '5':
	case '6':
		showBody[key - 51] = !showBody[key - 51];
	}
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

float ofApp::getCost()
{
	auto ret = 0.;

	auto c = (cams[0].cameraToWorld(bodies[0].cog) - cams[1].cameraToWorld(bodies[1].cog)).lengthSquared();

	for (int i=0; i < JointType_Count; i++) {
		ret += (cams[0].cameraToWorld(bodies[0].touchJoints[i].position) - cams[1].cameraToWorld(bodies[1].touchJoints[i].position)).lengthSquared();
	}
	

	return c;
}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
