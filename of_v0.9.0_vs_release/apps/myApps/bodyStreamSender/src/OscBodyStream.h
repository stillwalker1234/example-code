#pragma once

#include "ofMain.h"
#include "ofxKinect2.h"
#include "ofxOsc.h"
#include <fstream>

class OscBodyStream : public ofxKinect2::BodyStream
{
public:
	OscBodyStream() {
		if (readConfig()) {
			sender.setup(host, ofToInt(port));
			ofLogNotice("Pipe is ready...");
		}
	}
	~OscBodyStream() {}

	auto cam2of(CameraSpacePoint p) {
		return ofVec3f(p.X, p.Y, p.Z);
	}

	bool readFrame() {
		if (ofxKinect2::BodyStream::readFrame()) {
			auto msg = ofxOscMessage();
			msg.setAddress("/body");
			ofBuffer bodyBuffer, planeBuffer, orientBuffer;

			auto closestBody = 1000.;
			ofxKinect2::Body* b = NULL;
			for (auto& body : bodies) {
				if (body.isTracked()) {
					if (cam2of(body.joints[JointType_SpineMid].Position).lengthSquared() < closestBody) {
						closestBody = cam2of(body.joints[JointType_SpineMid].Position).lengthSquared();
						b = &body;
					}
				}
			}

			if (b == NULL)
				return false;
			
			bodyBuffer.append((const char*)&b->joints.front(), b->joints.size() * sizeof(Joint));
			orientBuffer.append((const char*)&b->joint_orients.front(), b->joint_orients.size() * sizeof(JointOrientation));
			planeBuffer.append((const char*)&getFloorPlane(), sizeof(ofVec4f));
			msg.addInt32Arg(ofToInt(id));
			msg.addBlobArg(planeBuffer);
			msg.addBlobArg(bodyBuffer);
			msg.addBlobArg(orientBuffer);

			sender.sendMessage(msg, false);

			return true;
		}

		return false;
	}

	

	bool readConfig(string name = "config.txt") {
		ifstream f_pointer;
		f_pointer.open(name);
		if (f_pointer.fail()) {
			ofLogError("cant open " + name + ", does it exist?");
			return false;
		}

		vector<string> ret;
		string line;
		while (f_pointer >> line) {
			ret.push_back(line);
		}

		if (ret.size() == 3) {
			host = ret[0];
			port = ret[1];
			id = ret[2];
		}
		else {
			ofLogError("config does not have the right format");
			return false;
		}

		return true;
	}

private:
	ofxOscSender sender;
	string host, port, id;
};
