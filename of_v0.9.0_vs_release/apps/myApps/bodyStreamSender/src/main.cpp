#include "ofMain.h"
#include "ofxKinect2.h"
#include "OscBodyStream.h"
#include "stdio.h"


class _Main
{
public:
	_Main() {
		_device = new ofxKinect2::Device();
		_device->setup();

		if (_body_stream.setup(*_device))
		{
			_body_stream.open();
		}
	};
	~_Main() {
		ofLogNotice("exiting");
		_body_stream.close();
		_device->exit();
		delete _device;
		_device = NULL;
	};

private:
	ofxKinect2::Device* _device;
	OscBodyStream _body_stream;
};


//========================================================================
int main() {

	_Main m;
	system("pause");
}
