#include "fftToMagFq.h"


fftToMagFq::fftToMagFq() : old(vector<float>())
{

}

fftToMagFq::~fftToMagFq()
{

}

void fftToMagFq::transform(vector<float>&in, float step)
{
	if (old.size() != in.size()) {
		old.resize(in.size(), 0.0f);
	}

	for (int i = 0; i < in.size() >> 1; ++i) {
		// calc actual phase
		float phase = atan2f(in[(i * 2) + 1], in[i * 2]);
		float aqFreq = getActualFq(phase - old[(i * 2) + 1], i, step);

		old[(i*2)+1] = phase;
		auto tmp = aqFreq - (float)i;
		bool sign = tmp > 0.0;
		if (abs(tmp) > 1.0)
			aqFreq = (float)i + (sign ? 1.0 : -1.0);

		in[i * 2] = sqrtf((in[(i * 2) + 1] * in[(i * 2) + 1]) + (in[i * 2] * in[i * 2])) / (float)(in.size() >> 1);
		in[(i * 2) + 1] = i / (float)(in.size());

	}
}

float fftToMagFq::getActualFq(float phaseChange, int binNum, float step)
{
	float tmp = phaseChange;

	tmp -= (float)(binNum)*TWO_PI* (1.0f / step);
	qpd = tmp / PI;
	if (qpd >= 0) qpd += qpd & 1;
	else qpd -= qpd & 1;
	tmp -= PI*(float)qpd;
	tmp *= step / TWO_PI;

	return (float)binNum + tmp;
}
