#include "VectorPrensenter.h"
#include "Helpers.h"
#include "StaticObjs.h"

SimplePresenter::SimplePresenter(VectorPresenter::display_type _display_type) : _display_type(_display_type)
{

}

SimplePresenter::~SimplePresenter()
{

}

void SimplePresenter::push(vector<float>& dat)
{
	points = dat;
}

void SimplePresenter::update()
{

}

void SimplePresenter::draw(ofRectangle& viewPort, float& dbFloor, ofPoint scroll, int size)
{
	if (points.size() == 0) return;

	float fmax = 0.0f;
	int fint = 0;
	width = 4.0f;
	ofSetColor(240);
	ofRect(viewPort);

	if (_display_type == VectorPresenter::LINE) {
		ofPolyline line;
		line.begin();		
		ofSetColor(0.0f);
		line.addVertex(ofVec2f(viewPort.x, viewPort.y + viewPort.height));
		for (int i = 0; i < points.size() >> 1; i++)
		{
			auto x = fqToPixel(points.size() >> 1, points[(i * 2) + 1], viewPort);
			auto y = viewPort.y + viewPort.height - (0.5f*(Helpers::volToDb( points[i * 2], -60.0f) * viewPort.height));
			line.addVertex(ofVec2f(x, y));
		}
		line.addVertex(ofVec2f(viewPort.x + viewPort.width, viewPort.y + viewPort.height));

		line.close();
		line.draw();
	}
	else if(_display_type == VectorPresenter::V_LINES) {
		for (int i = 0; i < points.size() >> 1; i++)
		{
			auto x = Helpers::fqToPixel(points[(i * 2) + 1], viewPort);
			auto y = points[i * 2];
			ofSetColor(1.0 - y);
			ofSetLineWidth(width);
			ofLine(
				x,
				viewPort.y + (0.5f*viewPort.height),
				x,
				viewPort.y + (0.5f*viewPort.height) - (0.5f*y*viewPort.height));
		}
	}
	

	// ofDrawBitmapString(ofToString(m*dbFloor), viewPort.x-18.0f, viewPort.y + (viewPort.height*0.5f));
}

float SimplePresenter::fqToPixel(int size, float in, ofRectangle& viewPort)
{
	if (in < .0f) return 0.0f;
	float fftInv = 1.f / size;
	return ((log10f((in + fftInv)*size) / log10f(fftInv + size)) * viewPort.width) + viewPort.x;
}

SimplePresenterVBO::SimplePresenterVBO()
{
	ofSetVerticalSync(false);
	shader.setGeometryInputType(GL_POINTS);
	shader.setGeometryOutputType(GL_LINE_STRIP);
	shader.setGeometryOutputCount(2);
	shader.load("shaders/noise.vert", "shaders/noise.frag", "shaders/noise.geom");
	doShader = true;
}

SimplePresenterVBO::~SimplePresenterVBO()
{
}

void SimplePresenterVBO::push(vector<float>& dat)
{
	points = dat;
}

void SimplePresenterVBO::update()
{

}

void SimplePresenterVBO::draw(ofRectangle& viewPort, float& dbFloor, ofPoint scroll, int size)
{
	vbo.setVertexData(&points[1], 2, points.size() / 2 - 2, GL_STREAM_DRAW, 2 * sizeof(float));

	if (doShader) {
		shader.begin();

		shader.setUniform1f("baseline", viewPort.y + viewPort.height);
		shader.setUniform1f("dbFloor", dbFloor);
		shader.setUniform1i("fftSize", points.size());
		shader.setUniform4f("viewport", viewPort.x, viewPort.y, viewPort.width, viewPort.height);
	}


	vbo.draw(GL_POINTS, 0, points.size() / 2 - 2);
	if (doShader) shader.end();
}

float SimplePresenterVBO::fqToPixel(int size, float in, ofRectangle& viewPort)
{
	if (in < .0f) return 0.0f;
	float fftInv = 1.f / size;
	return ((log10f((in + fftInv)*size) / log10f(fftInv + size)) * viewPort.width) + viewPort.x;
}

SonoGram::SonoGram(int _steps /*= 500*/) : steps(_steps)
{
	shader.setGeometryInputType(GL_POINTS);
	shader.setGeometryOutputType(GL_LINE_STRIP);
	shader.setGeometryOutputCount(2);
	shader.load("shadersSonoGram/noise.vert", "shadersSonoGram/noise.frag", "shadersSonoGram/noise.geom");
	doShader = true;
}

SonoGram::~SonoGram()
{
}

void SonoGram::push(vector<float>& dat)
{
	if (dat.size() == 0)
		return;

	if (dat.size() != size) {
		size = dat.size();
		vbo.setVertexData(NULL, 2, (dat.size() / 2) * steps, GL_STREAM_DRAW, 2 * sizeof(float));
	}

	vbo.updateVertexData(&dat[1], (dat.size() / 2) * currentIdx, dat.size() / 2);
	currentIdx = (++currentIdx) % steps;
}

void SonoGram::update()
{

}

void SonoGram::draw(ofRectangle& viewPort, float& dbFloor, ofPoint scroll, int size)
{
	if (doShader) {
		shader.begin();

		// set thickness of ribbons
		shader.setUniform2f("scroll", scroll.x, scroll.y);
		shader.setUniform1f("baseline", viewPort.y + viewPort.height);
		shader.setUniform1f("dbFloor", dbFloor);
		shader.setUniform1i("fftSize", size);
		shader.setUniform1i("currentIdx", currentIdx);
		shader.setUniform1i("steps", steps);
		shader.setUniform4f("viewport", viewPort.x, viewPort.y, viewPort.width, viewPort.height);
		// make light direction slowly rotate
		//shader.setUniform3f("lightDir", sin(ofGetElapsedTimef() / 10), cos(ofGetElapsedTimef() / 10), 0);
	}


	vbo.draw(GL_POINTS, 0, size*steps);
	if (doShader) shader.end();

	ofDrawBitmapString("fq: " + ofToString(currentIdx), viewPort.x, viewPort.y);

}

float SonoGram::fqToPixel(int size, float in, ofRectangle& viewPort)
{
	if (in < .0f) return 0.0f;
	float fftInv = 1.f / size;
	return ((log10f((in + fftInv)*size) / log10f(fftInv + size)) * viewPort.width) + viewPort.x;
}
