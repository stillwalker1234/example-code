#include "fftSimple.h"



fftSimple::fftSimple(function<void(vector<float>& out)> _target, int stepSize, int numSteps, unique_ptr<Scaler>& _scaler) : stepSize(stepSize) , numSteps(numSteps), fftTrans(fftToMagFq())
{
	scaler = move(_scaler);
	target = _target;
	doAlocate = true;
	points.reserve(1 << 16);
}

void fftSimple::alocate()
{
	fftSize = stepSize * numSteps;
	mycfg = kiss_fftr_alloc(stepSize * numSteps, false, 0, 0);
	scaler->setSize(scaler->getSize());
	realocated = true;
}

fftSimple::~fftSimple()
{
}

void fftSimple::push(vector<float>& in)
{
	if (doAlocate) {
		doAlocate = false;
		alocate();
	}

	scaler->scale(in.data(), in.size(), [&](vector<float>& dat) {
		if (dat.size() != points.size()){
			points.resize(dat.size(),.0f);
		}
		kiss_fftr(mycfg, (kiss_fft_scalar*) dat.data(), (kiss_fft_cpx*)points.data());
		// (real, im) -> (mag, freq)
		fftTrans.transform(points, (float)numSteps);
		
		target(points);
		_candraw = true;
	});
}

void fftSimple::stepsChanged(int & x) {
	stepSize = 1 << x;
	doAlocate = true;
}

void fftSimple::numChanged(int & x) {
	numSteps = x;
	doAlocate = true;
}

void fftSimple::setupParams(ofxPanel* gui)
{
	gui->add(numStepsParam.set("numSteps", 1, 8, 32));
	gui->add(stepSizeParam.set("stepSize", 8, 1, 14));
	stepSizeParam.addListener(this, &fftSimple::stepsChanged);
	numStepsParam.addListener(this, &fftSimple::numChanged);
}

int fftSimple::getOutSize()
{
	return stepSize * numSteps;
}

bool fftSimple::canDraw()
{
	return _candraw;
}
