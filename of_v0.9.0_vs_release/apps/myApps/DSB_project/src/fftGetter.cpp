#include "fftGetter.h"

fftGetter::fftGetter() : r1(ofRectangle()), _fft(new fftSimple([&](vector<float>& in) 
{ 
	if (in.size() != points.size())
		points.resize(in.size());

	count+=1.0f;
	for (int i = 0; i < in.size()>>1; ++i)
	{
		points[i*2] = ((1.0f / count)*in[i*2]) + (((count - 1.0f) / count)*points[i*2]);
		points[(i * 2) + 1] = in[(i * 2) + 1];
	}
	s1.push(points); 
}, 2048, 1, unique_ptr<Scaler>(new Scaler(2048*2))))
{
	Helpers::fqToPixel = [&](float in, ofRectangle viewport) {
		if (in < .0f) return 0.0f;
		return (
			(log10f((in)*44100) /
				log10f(44100)) *
			viewport.width) + viewport.x;
	};
}

fftGetter::~fftGetter()
{
}

void fftGetter::setup(ofRectangle view)
{
	setupGui(view);
}

void fftGetter::update()
{
}

void fftGetter::draw()
{
	s1.draw(viewPort, floor, ofPoint(0.0, 0.0), _fft->getOutSize());
}

void fftGetter::exit()
{
	delete _fft.release();
}

void fftGetter::keyPressed(int key)
{
	if (key == 'c')
		count = 0.0;
}

void fftGetter::keyReleased(int key)
{
}

void fftGetter::mouseMoved(int x, int y)
{
}

void fftGetter::mouseDragged(int x, int y, int button)
{
}

void fftGetter::mousePressed(int x, int y, int button)
{
}

void fftGetter::mouseReleased(int x, int y, int button)
{
}

void fftGetter::dragEvent(ofDragInfo dragInfo)
{
}

void fftGetter::gotMessage(ofMessage msg)
{
}

void fftGetter::audioOut(float * output, int bufferSize, int nChannels)
{
}

float fftGetter::getPerferedHeight()
{
	return 150.0f;
}

void fftGetter::push(vector<float>& in)
{
	vector<float> out(in.size()>>1, 0.0);
	for (int i = 0; i < out.size(); i++)
	{
		out[i] = (in[i * 2] + in[(i * 2) + 1]) / 2.0f;
	}
	_fft->push(out);
}

void fftGetter::clearCount()
{
	_fft->alocate();
	count = 0;
}

vector<float> fftGetter::getPoints()
{
	return points;
}

