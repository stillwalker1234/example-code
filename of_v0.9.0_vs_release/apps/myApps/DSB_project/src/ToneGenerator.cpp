#include "ToneGenerator.h"
#include "Helpers.h"
#include "StaticObjs.h"
#define M_PI 3.14159265358979323846

ToneGenerator::ToneGenerator() : setupDone(0)
{
}


ToneGenerator::~ToneGenerator()
{
	
}


void ToneGenerator::setup(ofRectangle _view){
	setupGui(_view);
	vol.addListener(this, &ToneGenerator::volChanged);
	gui.add(freq.set("Frequency", 10900, 10, 20000));
	gui.add(vol.set("Volume", .75, 0.0, 1.0));
	gui.add(db.set("db: ", ""));
	setupDone = true;
}

void ToneGenerator::volChanged(float & vol) {
	db = ofToString(log10f(vol) * 20.0f);
}

float ToneGenerator::getPerferedHeight()
{
	return 120.0f;
}

//--------------------------------------------------------------
void ToneGenerator::exit(){
}

//--------------------------------------------------------------
void ToneGenerator::update(){
}

//--------------------------------------------------------------
void ToneGenerator::draw(){
	gui.draw();
}

//--------------------------------------------------------------
void ToneGenerator::keyPressed(int key){
}

//--------------------------------------------------------------
void ToneGenerator::keyReleased(int key){
}

//--------------------------------------------------------------
void ToneGenerator::mouseMoved(int x, int y){
}

//--------------------------------------------------------------
void ToneGenerator::mouseDragged(int x, int y, int button){
}

//--------------------------------------------------------------
void ToneGenerator::mousePressed(int x, int y, int button){
}

//--------------------------------------------------------------
void ToneGenerator::mouseReleased(int x, int y, int button){
}

//--------------------------------------------------------------
void ToneGenerator::gotMessage(ofMessage msg){
}

//--------------------------------------------------------------
void ToneGenerator::dragEvent(ofDragInfo dragInfo){
}

void ToneGenerator::audioOut(float* output, int bufferSize, int nChannels){
	if (setupDone){

	while (phase > TWO_PI){
		phase -= TWO_PI;
	}
	phaseAdder = 0.95f * phaseAdder + 0.05f * ((freq.get() / (float)StaticObjs::soundStream.getSampleRate()) * TWO_PI);
	float current = 0;
	for (int i = 0; i < bufferSize; i++)
	{
		phase += phaseAdder;
		current = sin(phase) * vol.get();
		for (int c = 0; c < nChannels; ++c)
		{
			output[i*nChannels + c] += current;
		}
	}
	}
}
