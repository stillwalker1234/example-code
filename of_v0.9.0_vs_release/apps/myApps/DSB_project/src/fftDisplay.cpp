#include "fftDisplay.h"
#include "ofComponentStack.h"
#include "fftRecursive.h"
#include "Helpers.h"

	

fftDisplay::fftDisplay(int _fftSize, int windowsFactor) : scroll(0.0f,0.0f)
{
	fftProc = unique_ptr<fftRaw>(new fftRecursive([&](vector<float> out) { points = out; }, _fftSize, windowsFactor, 5));
	points.resize(0);

	Helpers::fqToPixel = [&](float in, ofRectangle viewport) {
		if (in < .0f) return 0.0f;
		assert(in < 0.5f && in >= 0.0f);
		float fftInv = 1.f / fftProc->getOutSize();
		float ftP = (
			(log10f((in + fftInv)*fftProc->getOutSize()) /
			log10f(fftInv + fftProc->getOutSize())) *
			viewport.width) + viewport.x;
		return (ftP - scroll.x) * scroll.y + ftP;
	};
	xScale = unique_ptr<UIScale>(new _xScale(vector<float> { 20, 50, 100, 200, 500, 1000, 2000, 5000, 10000,20000 }, viewPort, Helpers::fqToPixel));

} 

fftDisplay::~fftDisplay()
{
	
	bool a = 0;
}

void fftDisplay::exit() {

}
void fftDisplay::setup(ofRectangle _view){
	setupGui(_view);
	dbFloorParam.addListener(this, &fftDisplay::dbFloorChanged);
	gui.add(dbFloorParam.set("Floor", -100.0f, -60.0f, -30.0f));
	sonogram = unique_ptr<SonoGram>(new SonoGram(100));
	fftProc->setupParams(&gui);
}

//--------------------------------------------------------------
void fftDisplay::update(){
	sonogram->push(points);
}

//--------------------------------------------------------------
void fftDisplay::draw(){
	gui.draw();

	if (!fftProc->canDraw())
		return;

	ofSetColor(0);
	
	sonogram->draw(viewPort, dbFloor, scroll, fftProc->getOutSize());
	//presenter2->draw(viewPort, dbFloor, scroll, proc1->getOutSize());
	xScale->draw();

	float fmax = 0;
	float idxmax = 0;
	for (int i = 0; i < points.size(); i+=2)
	{
		if (points[i]>fmax) {
			fmax = points[i];
			idxmax = points[i+1];
		}
	}

}
#pragma region stuff

//--------------------------------------------------------------
void fftDisplay::keyPressed(int key){
}

//--------------------------------------------------------------
void fftDisplay::keyReleased(int key){
}

//--------------------------------------------------------------
void fftDisplay::mouseMoved(int x, int y){
}

//--------------------------------------------------------------
void fftDisplay::mouseDragged(int x, int y, int button){
}

//--------------------------------------------------------------
void fftDisplay::mousePressed(int x, int y, int button){
	ofLogNotice("scroll" + ofToString(button));
	if (button == oldBut) {
		oldBut = -1;
	}
	else if ((button == 4 || button == 3)) {
		scroll.x = (x - viewPort.x);
		scroll.y += button == 4 ? 0.1 : -0.1;
		oldBut = button;
	}
}

//--------------------------------------------------------------
void fftDisplay::mouseReleased(int x, int y, int button){
}
#pragma endregion stuff

//--------------------------------------------------------------
void fftDisplay::windowResized(int w, int h){
	xScale->resized(w,h);
}

//--------------------------------------------------------------
void fftDisplay::gotMessage(ofMessage msg){
}

//--------------------------------------------------------------
void fftDisplay::dragEvent(ofDragInfo dragInfo){
}

void fftDisplay::audioOut(float* output, int bufferSize, int nChannels){
	vector<float> v(bufferSize);
	for (int i = 0; i < bufferSize; ++i)
	{
		float out = 0;
		for (int c = 0; c < nChannels; c++)
		{
			out += output[i*nChannels + c];
		}
		v[i] = out / (float)nChannels;
	}
	fftProc->push(v);
}


float fftDisplay::getPerferedHeight()
{
	return 200.0f;
}

void fftDisplay::dbFloorChanged(float & pressed)
{
	dbFloor = pressed;
}

fftDisplay::_xScale::_xScale(vector<float>& frq, ofRectangle& _view, function<float(float, ofRectangle)>& fc) : fqToPx(fc), view(_view), fq(frq)
{

}

void fftDisplay::_xScale::draw()
{
	for (int i = 0; i < fq.size(); i++)
		ofDrawBitmapString(ofToString(fq[i]), fqToPx(fq[i] / StaticObjs::soundStream.getSampleRate(), view), view.y + view.height);	
}

void fftDisplay::_xScale::resized(int w, int h)
{

}
