#pragma once
#include "ofMain.h"
#include "ofxGui.h"
class fftRaw
{
public:
	virtual ~fftRaw(){}

	virtual void push(vector<float>& in) = 0;

	virtual void setupParams(ofxPanel* gui) = 0;

	virtual int getOutSize() = 0;

	virtual bool canDraw() = 0;




};