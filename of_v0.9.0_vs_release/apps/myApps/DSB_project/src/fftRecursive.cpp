#include "fftRecursive.h"
#include "ofComponentStack.h"


fftRecursive::fftRecursive(function<void(vector<float>& out)> _target, int stepSize, int steps,int _levels)
{
	target = _target;
	levels = _levels;
	filters.resize(levels);
	for (int i = 0; i < levels; i++)
	{
		ffts.push_back(unique_ptr<fftSimple>(new fftSimple([=](
			vector<float>& out) { combine(out, i); }, 
			stepSize / (1 << i), (1 << i)*steps,
			unique_ptr<Scaler>(new ofxWindowScaler(stepSize / (1 << i), (1 << i)*steps)))));
		filters[i].setup(16,    // order
			SAMPLE_RATE,// sample rate
			(SAMPLE_RATE>>2)-(SAMPLE_RATE>>5), // center frequency
			//880,  // band width
			.001,
			4);   // ripple dB
		//f.process(numSamples, arrayOfChannels);
	
	}
	sendSize = (stepSize*steps / 2) * (levels + 1);
	sendBlock = vector<float>(sendSize, 0.0f);
}

fftRecursive::~fftRecursive()
{

}

void fftRecursive::push(vector<float>& in)
{
	auto v = in;

	for (int i = 0; i < ffts.size(); i++)
	{
		ffts[i]->push(v);
		filters[i].process(v.size(), (vector < float* > {v.data() }).data());
		for (int i = 0; i < v.size() >> 1; ++i)
		{
			v[i] = v[i * 2];
		}
		v.resize(v.size() >> 1);
	}
}

void fftRecursive::setupParams(ofxPanel* gui)
{
}

int fftRecursive::getOutSize()
{
	return sendSize/2;
}

bool fftRecursive::canDraw()
{
	return _canDraw;
}

void fftRecursive::combine(vector<float>& out, int level)
{
	

	int num = out.size();
	int binSize = out.size() / 2;
	
	int offset = out.size() / offsetDiv;

	for (int i = 0; i < (binSize); ++i)
	{
		out[((i * 2) + 1)] /= (float)((1 << level));
	}
	/*
	*/
	copy(
		out.begin() + (binSize - offset),
		out.end() - (level ? offset : 0),
		sendBlock.end() - (binSize*(level + 1) + offset));

	if (level == (levels - 1)) {
		copy(
			out.begin(), out.end() - binSize - offset, 
			sendBlock.begin());
		target(sendBlock);
		_canDraw = true;
	}
}
