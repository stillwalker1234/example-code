#include "ofMain.h"

class Helpers
{
public:

	static float volToDb(float p, float dbFloor)
	{
		auto val = log10(p)*20.0f;
		return val < dbFloor ? 0.0 : 1.0f - (val / dbFloor);
	}

	static function<float(float, ofRectangle)> fqToPixel;
private:

};

__declspec(selectany) function<float(float, ofRectangle)> Helpers::fqToPixel;

