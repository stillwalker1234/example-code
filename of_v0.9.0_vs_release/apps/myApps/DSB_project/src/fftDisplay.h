#pragma once
#include "ofMain.h"
#include "IComponent.h"
#include "kiss\kiss_fftr.h"
#include "ofxWindowScaler.h"
#include "fftRaw.h"
#include "VectorPrensenter.h"

class UIScale {
public:
	virtual void draw() = 0;
	virtual void resized(int,int) = 0;
};

class fftDisplay :
	public IComponent
{
public:
	fftDisplay(int _fftSize, int windowsFactor);
	~fftDisplay();

	void setup(ofRectangle view);
	void update();
	void draw();

	float fqToPxl(float in);

	void exit();
	void keyPressed(int key);
	void keyReleased(int key);
	void mouseMoved(int x, int y);
	void mouseDragged(int x, int y, int button);
	void mousePressed(int x, int y, int button);
	void mouseReleased(int x, int y, int button);
	void windowResized(int w, int h);
	void dragEvent(ofDragInfo dragInfo);
	void gotMessage(ofMessage msg);
	void audioOut(float * output, int bufferSize, int nChannels);

	float getPerferedHeight();
	void dbFloorChanged(float & pressed);
private:
	ofPoint scroll;
	unique_ptr<fftRaw> fftProc;
	unique_ptr<UIScale> xScale, yScale;
	unique_ptr<SonoGram> sonogram;
	vector<float> points;
	float dbFloor = -60.0f;
	int oldBut;
	ofParameter<float> dbFloorParam;
	class _xScale : public UIScale {
	public:
		_xScale(vector<float>& frq, ofRectangle&, function<float(float, ofRectangle)>&);

		~_xScale(){}
		void draw();
		void resized(int w, int h);
	private:
		vector<float> fq;
		function<float(float, ofRectangle)>& fqToPx;
		ofRectangle& view;
	};
};

