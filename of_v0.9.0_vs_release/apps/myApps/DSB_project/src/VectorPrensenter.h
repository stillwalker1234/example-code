#pragma once

#include "ofMain.h"


class VectorPresenter
{
public:
	// virtual ~VectorPresenter() = 0;
	virtual void push(vector<float>& dat) = 0;

	virtual void update() = 0;
	virtual void draw(ofRectangle& viewPort,
		float& dbFloor,
		ofPoint scroll,
		int size) = 0;

	enum display_type {
		V_LINES,
		LINE
	};
};

class SimplePresenter :
	public VectorPresenter
{
public:
	SimplePresenter(VectorPresenter::display_type _display_type);
	~SimplePresenter();
	void push(vector<float>& dat);

	void update();
	void draw(ofRectangle& viewPort,
		float& dbFloor,
		ofPoint scroll,
		int size);
protected:
	vector<float> points;
	float width;
private:
	VectorPresenter::display_type _display_type;
	float fqToPixel(int size, float in, ofRectangle& viewPort);
};

class SimplePresenterVBO :
	public VectorPresenter {

public:
	SimplePresenterVBO();
	~SimplePresenterVBO();
	void push(vector<float>& dat);

	 void update();

	 void draw(ofRectangle& viewPort, float& dbFloor, ofPoint scroll, int size);
protected:
	vector<float> points;

private:
	ofTrueTypeFont font;
	bool doShader = true;
	ofVbo vbo;
	ofShader shader;

	float fqToPixel(int size, float in, ofRectangle& viewPort);
};


class SonoGram :
	public VectorPresenter {

public:
	SonoGram(int _steps = 500);
	~SonoGram();
	void push(vector<float>& dat);

	void update();

	void draw(ofRectangle& viewPort, float& dbFloor, ofPoint scroll, int size);
private:
	int steps, size, currentIdx = 0;
	ofTrueTypeFont font;
	bool doShader = true;
	ofVbo vbo;
	ofShader shader;

	float fqToPixel(int size, float in, ofRectangle& viewPort);
};