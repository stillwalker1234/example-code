#include "FIR_module.h"

FIR_module::FIR_module() : disp(vector<SimplePresenter>(2,SimplePresenter(VectorPresenter::LINE)))
{
	impulse_display_cfg = kiss_fftr_alloc(display_size, false, 0, 0);
	impulse_disp.resize(display_size,0.0f);
}

FIR_module::~FIR_module()
{
	/*
	*/
}

void FIR_module::set(vector<float>& in)
{
	if (in.size() != input.size()) {
		input.resize(in.size(),0.0f);
		w.resize(in.size(), 0.0f);
		impulse_time.resize(in.size() >> 1, 0.0f);
		impulse_cfg = kiss_fftr_alloc(in.size()>>1, true, 0, 0);
	}
	

	for (int i = 0; i < input.size() >> 1; i++)
	{
		// set real part
		input[i * 2] = in[i * 2];
	}
	kiss_fftri(impulse_cfg, (kiss_fft_cpx*)input.data(), (kiss_fft_scalar*)impulse_time.data());
	rotate(impulse_time.begin(), impulse_time.begin() + (impulse_time.size() >> 1), impulse_time.end());
	
	applyKaiser(_beta);
}

void FIR_module::setup(ofRectangle view)
{
	setupGui(view);
}

void FIR_module::update()
{
}

void FIR_module::draw()
{
	auto h = viewPort.height / disp.size();
	int k = 0;
	for (int i = 0; i < viewPort.height; i+=h)
	{
		auto rect = ofRectangle(viewPort.x, i + viewPort.y, viewPort.width, h);
		disp[k].draw(rect, floor, ofPoint(), 0);
		k++;
	}
}

void FIR_module::exit()
{
	free(impulse_cfg);
	free(impulse_display_cfg);
	free(FIR_FFT_cfg_forward);
	free(FIR_FFT_cfg_backward);
}

void FIR_module::keyPressed(int key)
{
	if (key == 'o')
		on = !on;

	if (key == 'i') {
		_beta += 6.0f;
		applyKaiser(_beta);
	}
	else if (key == 'u') {
		if (_beta > 0.0f)
			_beta -= 6.0f;

		applyKaiser(_beta);
	}

	printf("beta: %f", _beta);
}

void FIR_module::keyReleased(int key)
{
}

void FIR_module::mouseMoved(int x, int y)
{
}

void FIR_module::mouseDragged(int x, int y, int button)
{
}

void FIR_module::mousePressed(int x, int y, int button)
{
}

void FIR_module::mouseReleased(int x, int y, int button)
{
}

void FIR_module::dragEvent(ofDragInfo dragInfo)
{
}

void FIR_module::gotMessage(ofMessage msg)
{
}

void FIR_module::audioOut(float * output, int bufferSize, int nChannels)
{
	if (impulse_time.size() == 0 || !on)
		return;

	auto kernelSize = bufferSize + impulse_time.size() - 1;

	int closestPowerOf2 = 2;
	while (closestPowerOf2 < kernelSize)
		closestPowerOf2 = closestPowerOf2 << 1;
	kernelSize = closestPowerOf2;

	if (_kernelSize != kernelSize) {
		signal_buffer.resize(kernelSize, 0.0f);
		signal_buffer_prev.resize(kernelSize, 0.0f);
		signal_buffer_freq.resize(kernelSize, 0.0f);
		_kernelSize = kernelSize;

		resetImpulseFreq = true;
	}

	if (resetImpulseFreq) {

		auto tmp = impulse_time;

		for (int i = 0; i < tmp.size(); i++)
		{
			tmp[i] *= w[i];
		}

		tmp.resize(kernelSize, 0.0f);
		impulse_freq.resize(kernelSize, 0.0f);

		FIR_FFT_cfg_forward = kiss_fftr_alloc(kernelSize, false, 0, 0);
		FIR_FFT_cfg_backward = kiss_fftr_alloc(kernelSize, true, 0, 0);
		for (int i = 0; i < tmp.size(); i++)
		{
			tmp[i] /= (tmp.size()>>1);
		}

		kiss_fftr(FIR_FFT_cfg_forward, (kiss_fft_scalar*)tmp.data(), (kiss_fft_cpx*)impulse_freq.data());
		impulse_disp = impulse_freq;

		for (int i = 0; i < impulse_disp.size() >> 1; i++)
		{
			impulse_disp[i * 2] = sqrtf((impulse_disp[(i * 2) + 1] * impulse_disp[(i * 2) + 1]) + (impulse_disp[i * 2] * impulse_disp[i * 2]));
			impulse_disp[(i * 2) + 1] = (float)i / (float)(impulse_disp.size() >> 1);
		}

		disp[0].push(impulse_disp);


		resetImpulseFreq = false;
	}

	// signal_buffer = sig_i-1
	signal_buffer_prev = signal_buffer;

	for (int i = 0; i < kernelSize; i++)
	{
		signal_buffer[i] = 0.0f;
		if (i < bufferSize) {
			for (int k = 0; k < nChannels; k++)
			{
				// copy in new
				signal_buffer[i] += output[i + k];			
				output[i + k] = (i<(kernelSize - bufferSize)) ? ( signal_buffer_prev[bufferSize+i] / kernelSize ): 0.0f;
			}
			signal_buffer[i] = signal_buffer[i]/(float)nChannels;
		}
	}
	// time -> freq
	kiss_fftr(FIR_FFT_cfg_forward, (kiss_fft_scalar*)signal_buffer.data(), (kiss_fft_cpx*)signal_buffer_freq.data());
	
	// freq domain convolve
	for (int k = 0; k < signal_buffer_freq.size()>>1; k++)
	{
		int i = k * 2;
		auto a = signal_buffer_freq[i];
		auto imp_r = impulse_freq[i];
		auto imp_i = impulse_freq[i + 1];
		signal_buffer_freq[i] = (a * imp_r) - (signal_buffer_freq[i + 1] * imp_i);
		signal_buffer_freq[i+1] = (a * imp_i) + (signal_buffer_freq[i + 1] * imp_r);
	}

	// freq -> time
	kiss_fftri(FIR_FFT_cfg_backward, (kiss_fft_cpx*)signal_buffer_freq.data(), (kiss_fft_scalar*)signal_buffer.data());

	// signal_buffer = sig_i
	for (int i = 0; i < bufferSize; i++)
	{
		for (int k = 0; k < nChannels; k++)
		{
			output[i + k] += signal_buffer[i] / kernelSize;
			clamp(output[i + k]);
		}
	}
}

float FIR_module::getPerferedHeight()
{
	return 300.0f;
}

void FIR_module::applyKaiser(float beta)
{

	auto error_margin = 1E-21;
	auto NOMBF = [=](float in) {
		float temp;
		float sum = 1.0f;
		float temp_2 = 1.0f;
		float half_in = in / 2.0f;
		int m = 1;

		do {
			temp = half_in / (float)m;
			temp_2 *= temp * temp;
			sum += temp_2;
			m++;
		} while (temp_2 >= error_margin * sum);
		return sum;
	};

	int n = impulse_time.size();
	float temp;
	float denominator = NOMBF(beta);
	int k2 = 1 - (n & 1); // 1 if n is even
	int loop_end = (n + 1) >> 1;

	for (int i = 0; i < loop_end; i++)
	{
		temp = (float)(2 * i + k2) / (float)(n - 1);
		auto value = NOMBF(beta*sqrtf(1.0f - temp*temp)) / denominator;
		w[loop_end - (1 & (!k2)) + i] = value;
		w[loop_end - 1 - i] = value;
	}

	resetImpulseFreq = true;
}

void FIR_module::createImpFrq(int size)
{
}

void FIR_module::clamp(float& in)
{
	if (in > 1.0f)
		in = 1.0f;
	else if (in < -1.0f)
		in = -1.0f;
}
