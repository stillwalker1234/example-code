#pragma once
#include "ofMain.h"
#include "fftRaw.h"
#include "kiss\kiss_fftr.h"
#include "ofxWindowScaler.h"
#include "fftToMagFq.h"

class fftSimple : public fftRaw
{
public:
	fftSimple(function<void(vector<float>& out)> _target, int stepSize, int numSteps, unique_ptr<Scaler> & _scaler);

	void alocate();

	~fftSimple();

	void push(vector<float>& in) override;

	void stepsChanged(int & x);
	void numChanged(int & x);
	void setupParams(ofxPanel* gui) override;

	int getOutSize() override;

	bool canDraw() override;

private:
	kiss_fftr_cfg mycfg;
	function < void( vector<float>& out) > target;
	unique_ptr<Scaler> scaler;
	fftToMagFq fftTrans;
	int fftSize, numSteps, stepSize;
	vector<float> points;
	ofParameter<int> stepSizeParam, numStepsParam;
	bool doAlocate;
	bool realocated;
	bool _candraw = false;
};