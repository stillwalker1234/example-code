#include "Common.h"
#include "Filter.h"
#include "ChebyshevI.h"
#include "Elliptic.h"
#include "fftRaw.h"
#include "fftSimple.h"



class fftRecursive
	: public fftRaw
{
public:
	fftRecursive(function<void(vector<float>& out)> _target, int stepSize, int steps,int levels);

	~fftRecursive();

	virtual void push(vector<float>& in) override;

	virtual void setupParams(ofxPanel* gui) override;

	virtual int getOutSize() override;

	virtual bool canDraw() override;
	void combine(vector<float>& out, int i);
private:
	const int offsetDiv = 12;

	vector<Dsp::SimpleFilter<Dsp::Elliptic::LowPass<16>, 1>> filters;
	vector<unique_ptr<fftSimple>> ffts;
	int sendSize;
	vector<float> sendBlock;
	int levels;
	function<void(vector<float> out)> target;
	bool _canDraw = 0;
};
