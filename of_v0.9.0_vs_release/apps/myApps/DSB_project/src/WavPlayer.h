#pragma once

#include "ofMain.h"
#include "ofxWaveHandler.h"
#include "IComponent.h"
#define WAVEBUFFER_MINSEC 60

#define PORT 12345

class WavPlayer : public IComponent {

public:
	WavPlayer();
	WavPlayer(function<void(vector<float> data)> target);
	~WavPlayer();
	void setup(ofRectangle view);
	void update();
	void draw();
	void exit();

	void keyPressed(int key);
	void keyReleased(int key);
	void mouseMoved(int x, int y);
	void mouseDragged(int x, int y, int button);
	void mousePressed(int x, int y, int button);
	void mouseReleased(int x, int y, int button);
	void windowResized(int w, int h);
	void dragEvent(ofDragInfo dragInfo);
	void gotMessage(ofMessage msg);
	void audioOut(float * output, int bufferSize, int nChannels);
	void advance(int num);
	float getPerferedHeight();
	bool IsPlaying() const { return isPlaying; }
	void IsPlaying(bool val) { isPlaying = val; }
	void setOnline(bool val) { online = val; }
	void loadAndPlay(string path);
	ofEvent<bool> fileFinEvent;

private:
	function<void(vector<float>)> target;
	int	currentSlot, playPosition;
	bool isRecording, isPlaying, online;
	// Audio
	ofxWaveHandler* waveObject;
	int	waveStart, waveLength, meshDetail, waveCurrent;
	bool isStart = false;
};
