#pragma once

#include "ofMain.h"
#include "IComponent.h"
#include "VectorPrensenter.h"
#include "fftSimple.h"
#include "Helpers.h"


class fftGetter : public IComponent
{
public:
	fftGetter();
	~fftGetter();

	// Inherited via IComponent
	virtual void setup(ofRectangle view) override;

	virtual void update() override;

	virtual void draw() override;

	virtual void exit() override;

	virtual void keyPressed(int key) override;

	virtual void keyReleased(int key) override;

	virtual void mouseMoved(int x, int y) override;

	virtual void mouseDragged(int x, int y, int button) override;

	virtual void mousePressed(int x, int y, int button) override;

	virtual void mouseReleased(int x, int y, int button) override;

	virtual void dragEvent(ofDragInfo dragInfo) override;

	virtual void gotMessage(ofMessage msg) override;

	virtual void audioOut(float * output, int bufferSize, int nChannels) override;

	virtual float getPerferedHeight() override;

	// other

	void push(vector<float>& in);

	void clearCount();
	vector<float> getPoints();
private:
	SimplePresenter s1 = SimplePresenter(VectorPresenter::LINE);
	ofRectangle r1;
	unique_ptr<fftSimple> _fft;
	vector<float> points;
	float count = 0;
	int size = 1024;
	float floor = -60.0;
};
