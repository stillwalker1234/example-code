#include "ofApp.h"

ofApp::ofApp() :
	_fft_1(new fftGetter()),
	wav_1(new WavPlayer([&](vector<float> in) { _fft_1->push(in); })),
	_fft_2(new fftGetter()),
	wav_2(new WavPlayer([&](vector<float> in) { _fft_2->push(in); })),
	_fir(new FIR_module),
	sonogram_disp(new fftDisplay(1024, 1)),
	tone(new ToneGenerator())
{
	ofSetFrameRate(30);
	addComponent(tone.get());
	addComponent(wav_1.get());
	addComponent(_fft_1.get());
	addComponent(wav_2.get());
	addComponent(_fft_2.get());
	addComponent(_fir.get());
	addComponent(sonogram_disp.get());
	StaticObjs::soundStream.printDeviceList();
	StaticObjs::soundStream.setDeviceID(0);
	StaticObjs::soundStream.setup(this, NUM_CHANNELS, NUM_CHANNELS, SAMPLE_RATE, STREAM_BUFFER_SIZE, 4);
	
	wav_1->setOnline(false);
	wav_2->setOnline(true);

}

ofApp::~ofApp()
{
}

void ofApp::keyPressed(int key)
{
	if (key == '1') {
		wav_1->IsPlaying(true);
		_fft_1->clearCount();

		while (wav_1->IsPlaying()) {
			wav_1->advance(512);
		}
	}

	if (key == '2') {
		wav_2->setOnline(false);
		wav_2->IsPlaying(true);
		_fft_2->clearCount();
		while (wav_2->IsPlaying()) {
			wav_2->advance(512);
		}
		wav_2->setOnline(true);
	}

	if (key == '3') {
		auto v_1 = _fft_1->getPoints();
		auto v_2 = _fft_2->getPoints();
		float max = -100000;
		for (int i = 0; i < v_1.size() >> 1; i++)
		{
			if (v_1[i * 2] > max)
				max = v_1[i * 2];
		}
		vector<float> v_3(v_1.size());
		for (int i = 0; i < v_1.size()>>1; i++)
		{
			v_3[i*2] = (max -( v_2[i*2] - v_1[i*2]))/max;
			v_3[(i * 2) + 1] = v_1[(i * 2) + 1];
		}

		_fir->set(v_3);
	}

	if (key == 'q') {
		StaticObjs::soundStream.close();
	}
	ofComponentStack::keyPressed(key);
}
