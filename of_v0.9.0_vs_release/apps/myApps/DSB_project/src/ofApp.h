#pragma once

#define KISS_FFT_USE_ALLOCA


#include "ofMain.h"
#include "ofComponentStack.h"
#include "fftGetter.h"
#include "FIR_module.h"
#include "fftDisplay.h"
#include "ToneGenerator.h"

class ofApp : public ofComponentStack {

	public:
		ofApp();
		~ofApp();
		void keyPressed(int key);
private:
	unique_ptr<fftGetter> _fft_1;
	unique_ptr<WavPlayer> wav_1;
	unique_ptr<fftGetter> _fft_2;
	unique_ptr<WavPlayer> wav_2;
	
	unique_ptr<FIR_module> _fir;
	unique_ptr<fftDisplay> sonogram_disp;
	unique_ptr<ToneGenerator> tone;
};
