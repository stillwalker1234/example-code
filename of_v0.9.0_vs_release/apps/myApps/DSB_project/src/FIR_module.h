#pragma once

#include "ofMain.h"
#include "VectorPrensenter.h"
#include "IComponent.h"
#include "kiss\kiss_fftr.h"
#include "fftToMagFq.h"
#include "ofxWindowScaler.h"

class FIR_module : public IComponent
{
public:
	FIR_module();
	~FIR_module();

	void set(vector<float>& in);
	// Inherited via IComponent
	virtual void setup(ofRectangle view) override;
	virtual void update() override;
	virtual void draw() override;
	virtual void exit() override;
	virtual void keyPressed(int key) override;
	virtual void keyReleased(int key) override;
	virtual void mouseMoved(int x, int y) override;
	virtual void mouseDragged(int x, int y, int button) override;
	virtual void mousePressed(int x, int y, int button) override;
	virtual void mouseReleased(int x, int y, int button) override;
	virtual void dragEvent(ofDragInfo dragInfo) override;
	virtual void gotMessage(ofMessage msg) override;
	virtual void audioOut(float * output, int bufferSize, int nChannels) override;
	virtual float getPerferedHeight() override;

	// other
private:
	const int display_size = 1 << 15;
	bool on = true;
	bool resetImpulseFreq = false;
	int _kernelSize = 0;
	vector<SimplePresenter> disp;
	vector<float> input, impulse_time, impulse_disp, impulse_freq, signal_buffer, signal_buffer_prev, signal_buffer_freq, w;
	float floor = -60;
	float _beta = 0.0f;
	kiss_fftr_cfg impulse_cfg;
	kiss_fftr_cfg impulse_display_cfg;
	kiss_fftr_cfg FIR_FFT_cfg_forward;
	kiss_fftr_cfg FIR_FFT_cfg_backward;
	fftToMagFq trns;

	void applyKaiser(float beta);
	void createImpFrq(int size);
	void clamp(float& in);
};

