#include "ofComponentStack.h"

ofComponentStack::ofComponentStack() : comp(vector<IComponent*>())
{
	pad = 15;
}


ofComponentStack::~ofComponentStack()
{
}

//--------------------------------------------------------------
void ofComponentStack::setup(){
	StaticObjs::soundStream.start();

	for (int i = 0, h = 0; i < comp.size(); ++i, h += comp[i - 1]->getPerferedHeight()) {
		auto v = ofRectangle(pad, (pad * (i + 1)) + h, ofGetWidth() - (2 * pad), comp[i]->getPerferedHeight());
		comp[i]->setup(v);
	}
}

//--------------------------------------------------------------
void ofComponentStack::update(){
	
	for (int i = 0; i < comp.size(); ++i) {
		comp[i]->update();
	}
}

//--------------------------------------------------------------
void ofComponentStack::draw(){
	for (int i = 0; i < comp.size(); ++i) {
		
		comp[i]->draw();
	}
}

//--------------------------------------------------------------
void ofComponentStack::keyPressed(int key){
	if (key == 'p') {
		elToRemove.push_back(comp.size()-1);
		
	}
	else if (key == 'f') {
		ofToggleFullscreen();
#if 0

		comp.push_back(
			unique_ptr<IComponent>(
			new fftDisplay(&gui, ofRectangle(pad, (pad * 2) + 200, ofGetWidth() - (2 * pad), 200), 1 << 10, 4)));

			comp[comp.size()-1]->setup();
		
#endif

	}
	for (int i = 0; i < comp.size(); ++i) {
		comp[i]->keyPressed(key);
	}
}

//--------------------------------------------------------------
void ofComponentStack::keyReleased(int key){
	for (int i = 0; i < comp.size(); ++i) {
		comp[i]->keyReleased(key);
	}
}

//--------------------------------------------------------------
void ofComponentStack::mouseMoved(int x, int y){
	for (int i = 0; i < comp.size(); ++i) {
		comp[i]->mouseMoved(x, y);
	}
}

//--------------------------------------------------------------
void ofComponentStack::mouseDragged(int x, int y, int button){
	for (int i = 0; i < comp.size(); ++i) {
		comp[i]->mouseDragged(x, y, button);
	}
}

//--------------------------------------------------------------
void ofComponentStack::mousePressed(int x, int y, int button){
	if (button == 1) {

	}
	for (int i = 0; i < comp.size(); ++i) {
		comp[i]->mousePressed(x, y, button);
	}
}

//--------------------------------------------------------------
void ofComponentStack::mouseReleased(int x, int y, int button){
	for (int i = 0; i < comp.size(); ++i) {
		comp[i]->mousePressed(x, y, button);
	}
}

//--------------------------------------------------------------
void ofComponentStack::windowResized(int w, int h){
	for (int i = 0, h = 0; i < comp.size(); ++i, h += comp[i - 1]->getPerferedHeight()) {
		comp[i]->changeView(ofRectangle(pad, (pad * (i + 1)) + h, ofGetWidth() - (2 * pad), comp[i]->getPerferedHeight()));
	}
}

//--------------------------------------------------------------
void ofComponentStack::gotMessage(ofMessage msg){
	for (int i = 0; i < comp.size(); ++i) {
		comp[i]->gotMessage(msg);
	}
}

//--------------------------------------------------------------
void ofComponentStack::dragEvent(ofDragInfo dragInfo){
	for (int i = 0; i < comp.size(); ++i) {
		comp[i]->dragEvent(dragInfo);
	}
}

void ofComponentStack::audioOut(float* output, int bufferSize, int nChannels){
	for (int i = 0; i < comp.size(); ++i) {
		comp[i]->audioOut(output, bufferSize, nChannels);
	}
	while (elToRemove.size()) {
		comp.erase(comp.begin() + elToRemove.back());
		
		elToRemove.pop_back();
	}
}

void ofComponentStack::exit()
{
	StaticObjs::soundStream.close();
}

void ofComponentStack::addComponent(IComponent* c)
{
	comp.push_back(c);
}
