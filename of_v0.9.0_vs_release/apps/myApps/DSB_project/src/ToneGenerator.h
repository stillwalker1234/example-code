#pragma once
#include "IComponent.h"
#include "ofMain.h"

class ToneGenerator :
	public IComponent
{
public:
	ToneGenerator();
	~ToneGenerator();

	void setup(ofRectangle _viewPort);
	void update();
	void draw();
	void exit();

	void keyPressed(int key);
	void keyReleased(int key);
	void mouseMoved(int x, int y);
	void mouseDragged(int x, int y, int button);
	void mousePressed(int x, int y, int button);
	void mouseReleased(int x, int y, int button);
	void dragEvent(ofDragInfo dragInfo);
	void gotMessage(ofMessage msg);
	void audioOut(float * output, int bufferSize, int nChannels);

private:
	void volChanged(float&);

	float getPerferedHeight();

	ofParameter<float> freq;
	ofParameter<float> vol;
	ofParameter<string> db;
	float phase = 0.0f, phaseAdder = 0.0f;
	bool setupDone;
};

