#include "ofMain.h"
#include "IComponent.h"
#include "WavPlayer.h"
#include "StaticObjs.h"

#define NUM_CHANNELS 1
#define SAMPLE_RATE 44100
#define STREAM_BUFFER_SIZE 2048


#pragma once
class ofComponentStack : public ofBaseApp
{
public:
	ofComponentStack();
	~ofComponentStack();

	void setup();
	void update();
	void draw();

	void keyPressed(int key);
	void keyReleased(int key);
	void mouseMoved(int x, int y);
	void mouseDragged(int x, int y, int button);
	void mousePressed(int x, int y, int button);
	void mouseReleased(int x, int y, int button);
	void windowResized(int w, int h);
	void dragEvent(ofDragInfo dragInfo);
	void gotMessage(ofMessage msg);
	void audioOut(float * output, int bufferSize, int nChannels);

	void exit();
	void addComponent(IComponent* c);
protected:
	vector<IComponent*> comp;
	vector<int> elToRemove;
	int pad;
private:

};

