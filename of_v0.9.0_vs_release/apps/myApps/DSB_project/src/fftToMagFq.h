#pragma once

#include "ofMain.h"

class fftToMagFq
{
public:
	fftToMagFq();
	~fftToMagFq();

	void transform(vector<float>&in, float step);
private:
	vector<float> old;
	long qpd = 0;

	float getActualFq(float phaseChange, int binNum, float step);
};