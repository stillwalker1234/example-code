/*
  ==============================================================================

    Scaler.cpp
    Created: 20 Jan 2015 1:54:34am
    Author:  Jonas

  ==============================================================================
*/

#include "Scaler.h"


Scaler::Scaler(int _size) : size(_size), buffer(_size), outputOffset(0) {

}

Scaler::~Scaler() {}

void Scaler::scale(const float* data, int numOfFloats, std::function<void(vector<float>&)> target) {
	if (size < 1)
		return;

	int inputOffset = numOfFloats;
	while (inputOffset > 0) {
		int advance = std::min(inputOffset, (size - outputOffset));
		copy(data + (numOfFloats - inputOffset), data + (numOfFloats - inputOffset) + advance, &buffer[outputOffset]);
		outputOffset += advance;
		inputOffset -= advance;
		if (outputOffset == size) {
			outputOffset = 0;
			target(vector<float>(buffer));
		}
	}
}

int Scaler::getSize() {
	return size;
}

void Scaler::setSize(int _size) {
	size = _size;
	buffer.resize(_size, 0.0f);
	outputOffset = 0;
};