/*
  ==============================================================================

    WindowScaler.h
    Created: 30 Jan 2015 9:03:05pm
    Author:  Jonas

  ==============================================================================
*/

#ifndef WINDOWSCALER_H_INCLUDED
#define WINDOWSCALER_H_INCLUDED

#define M_PI 3.14159265358979323846
#include "Scaler.h"


class ofxWindowScaler : public ShiftingScaler {
public:
	ofxWindowScaler(int size = 1024, int _stepsizeFactor = 2, std::function<float(float in, float out)> wfunc = nullptr) :
		ShiftingScaler(size, _stepsizeFactor, wfunc) {
		wfunc = [=] (float i, float size) {
			float x = (i / size)*10.0f;
			return exp(-((log(x)*log(x))/(1.0f)));
		};
	};
	~ofxWindowScaler(){};
	int getSize();
	void scale(const float* data, int numberOfBytes, 
		std::function<void(vector<float>&)> target) override {
		Scaler::scale(data, numberOfBytes, [&](vector<float>& block) {
			
			int fSize = block.size() * stepsizeFactor;
			int fStep = block.size();
			if (blockSize != fSize) {
				blockSize = fSize;
				oldBlock = std::vector<float>(fSize,0.0f);
				currentStep = 0;
			}

			copy(block.begin(), block.end(), &oldBlock[currentStep * fStep]);
			
			currentStep = (currentStep + 1) % stepsizeFactor;
			block.resize(fSize,0.0f);
				copy(&oldBlock[currentStep * fStep], 
					(&oldBlock[currentStep * fStep]) + (fSize - (fStep * currentStep)), 
					block.begin());

				if (currentStep != 0)
					copy(oldBlock.data(), oldBlock.data() + (fStep*currentStep), 
						&block[fSize - (currentStep * fStep)]);
			

			//apply window
			for (int m = 0; m < fSize; m++)
			{
				block[m] *= (windowsFunc((float)m, (float)fSize));
			}

			target(block);
		});
	}

private:
	std::vector<float> oldBlock;
	bool first = true;
	int currentStep = 0;
	int blockSize = 0;
};



#endif  // WINDOWSCALER_H_INCLUDED
