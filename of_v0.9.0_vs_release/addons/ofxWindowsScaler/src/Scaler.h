/*
  ==============================================================================

    Scaler.h
    Created: 20 Jan 2015 1:54:34am
    Author:  Jonas

  ==============================================================================
*/
#include "ofMain.h"

#ifndef SCALER_H_INCLUDED
#define SCALER_H_INCLUDED

#define M_PI 3.14159265358979323846

class Scaler {
public:
	Scaler(int size = 1024);
	~Scaler();
	void setSize(int size);
	int getSize();
	virtual void scale(const float* data, int numberOfBytes, std::function<void (vector<float>&)> target);
private:
	vector<float> buffer;
	int size, outputOffset;
};

class ShiftingScaler : public Scaler
{
public:
	ShiftingScaler(int size = 1024, int _stepsizeFactor = 2, std::function<float(float in, float out)> wfunc = nullptr) :
		stepsizeFactor(_stepsizeFactor), Scaler(size), windowsFunc(wfunc) {
		if (nullptr == windowsFunc) {
			windowsFunc = [=](float in, float n) -> float {
				return -.5*cos(2.*M_PI*in / n) + .5;;
			};
		}
	};
	~ShiftingScaler() {}

protected:
	std::function<float(float in, float n)> windowsFunc;
	int stepsizeFactor;
};

#endif  // SCALER_H_INCLUDED
