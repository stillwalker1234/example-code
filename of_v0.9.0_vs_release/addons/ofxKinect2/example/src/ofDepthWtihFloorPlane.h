#pragma once
#include "ofMain.h"
#include "ofxKinect2.h"

class DepthWithFloorPlane : public ofxKinect2::DepthStream {
public:
	DepthWithFloorPlane() : ofxKinect2::DepthStream() {}

	void update();
};