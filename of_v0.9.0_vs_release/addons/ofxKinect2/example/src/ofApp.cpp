#include "ofApp.h"
#include "ofMain.h"
#include <queue>
//--------------------------------------------------------------



void ofApp::setup(){
	ofSetFrameRate(60);
	// ofSetFullscreen(TRUE);
	device_ = new ofxKinect2::Device();
	device_->setup();
	sender.setup("127.0.0.1", 9000);
	
	receiver.setup(9001);

	if(depth_.setup(*device_))
	{
		depth_.open();
	}

	if (color_.setup(*device_))
	{
		//color_.open();
	}

	if (ir_.setup(*device_))
	{
		// ir_.open();
	}
	
	if(mapper_.setup(*device_))
	{
		mapper_.isReady(1, 0);
	}

	if(body_stream_.setup(*device_))
	{
		body_stream_.open();
		bodies = vector<BodyWithForce>(body_stream_.getNumBodies());
		for (int i = 0; i < bodies.size(); i++)
		{
			bodies[i].setFloorPointer(&floorPlane);
		}
	}
    
}

//--------------------------------------------------------------
void ofApp::update(){
	device_->update();
	hasBodyTrack = false;

	if (body_stream_.isFrameNew()) {
		auto b = body_stream_.getBodies();
		for (int i = 0; i < b.size(); i++) {
			bodies[i].update(b[i]);

			if (b[i].isTracked()) {
				hasBodyTrack = true;
				sendMes(bodies[i].touchJoints[JointType_AnkleLeft], "/left");
				sendMes(bodies[i].touchJoints[JointType_AnkleRight], "/right");
	
				while (receiver.hasWaitingMessages()) {
					ofxOscMessage mes;
					receiver.getNextMessage(mes);

					if (mes.getAddress() == "/setCenter") {
						setCirkleCenter(bodies[i].cog);
						ofLogNotice(ofToString(cirkleCenter) + " - center is");
					}

					if (mes.getAddress() == "/setRadius") {
						setCirkleRadius(bodies[i].cog);
						ofLogNotice(ofToString(cirkleRadius) + " - radius is");
					}

					if (mes.getAddress() == "/resetFloor") {
						floorPlane = ofVec3f::zero();
						count = 0;
					}
				}
			}
		}

		if (!hasBodyTrack) {
			auto f = floorTrickBlock::forceData();
			f.force = 0.;
			f._state = TrackingState_Tracked;
			sendMes(f, "/left");
			sendMes(f, "/right");
		}

		floorPlane = body_stream_.getFloorPlane();
	}

	mapper_.setDepth(depth_);

}


void ofApp::setFloorPlane(ofVec3f A, ofVec3f B, ofVec3f C)
{
	if (count == limit) {
		ofLogNotice("floor finding is done...");
		count++;
	}

	if (count < limit) {
		auto n = (A - B).getCrossed(C - B).getNormalized();
		auto _floorPlane = ofVec4f(n);
		_floorPlane.w = (-1.*n.x*A.x) + (-1.*n.y*A.y) + (-1.*n.z*A.z);
		auto f_count = float(count);
		if (count > (limit/5)) {
			floorPlane = (((f_count - 1.) / f_count) * floorPlane) + ((1. / f_count) * _floorPlane);
		}

		if (count == (limit / 5)) {
			ofLogNotice("floor finding started...");
		}
		//floorPlane = _floorPlane;
		count++;
	}
}

void ofApp::sendMes(floorTrickBlock::forceData & p, string addrs) {
	if (p._state != TrackingState_Tracked)
		return;

	// distance from center and angle
	auto onFloor = floorTrickBlock::pointOnFloor(p.position, floorPlane) - cirkleCenter;
	auto ref = (-1.*cirkleCenter).getNormalized();
	auto d = onFloor.length();
	auto angle = acosf(ref.dot(onFloor/d));
	auto mes = ofxOscMessage();
	mes.setAddress(addrs);
	mes.addFloatArg(d / cirkleRadius);
	mes.addFloatArg(onFloor.x > 0. ? angle : -1.*angle + (2*3.14));
	mes.addFloatArg(p.position.z);
	mes.addFloatArg(p.force);
	sender.sendMessage(mes);
}
void ofApp::printJointToFloorDistance(JointType type, ofxKinect2::Body& body)
{
	Joint joint = body.getJoint(type);
	if (joint.TrackingState == TrackingState_Tracked)
	{
		CameraSpacePoint point = joint.Position;
		
		// ofDrawBitmapString(ofToString(floor_trick_block.getDistanceFromFloor(point)), ofPoint(ofGetWidth() - 100, 10));
	}
}

//--------------------------------------------------------------




ofPoint ofApp::toScreenPoint(ofVec2f point) {

	float screenPointX = static_cast<float>(point.x * ofGetWidth()) / 512;
	float screenPointY = static_cast<float>(point.y * ofGetHeight()) / 424;

	return ofPoint(screenPointX, screenPointY);
}



void ofApp::draw(){
	if (doDraw) {

	ofSetBackgroundColor(0, 255);
	// color_.draw();
	// depth_.draw();
	
	//int size = 40;
	// ir_.draw(1920 - 512, 1080 - 848);
	auto b = body_stream_.getBodies();
	for(int i = 0; i<body_stream_.getNumBodies(); i++)
	{
		if(b[i].isTracked())
		{
			//printJointToFloorDistance(JointType_FootLeft, bodies[i]);

			auto tsp = [&](ofVec3f p) { return toScreenPoint(mapper_.mapCameraToDepthSpace(p.x, p.y, p.z)); };
			auto cog_screenpoint = tsp(bodies[i].cog);
			auto g_force_sp = tsp(bodies[i].cog+(bodies[i].g_force*10.));
			auto v_force_sp = tsp(bodies[i].cog+(bodies[i].v_force*10.));
			auto sum_force_sp = tsp(bodies[i].cog+(bodies[i].sum_force*10.));

			// draw cog
			ofSetColor(ofColor::aliceBlue);
			ofDrawEllipse(toScreenPoint(mapper_.mapCameraToDepthSpace(bodies[i].cog.x, bodies[i].cog.y, bodies[i].cog.z)), 20., 20.);
			

			ofSetColor(ofColor::white);
			ofDrawLine(cog_screenpoint, sum_force_sp);
			// draw on floor
			auto f_point = tsp(floorTrickBlock::pointOnFloor(bodies[i].cog, floorPlane));
			ofDrawEllipse(f_point, 10., 10.);
			// draw pressure on grund
			ofSetColor(ofColor::red);
			for(auto touchjoint : bodies[i].touchJoints) {
				if (touchjoint._state == TrackingState_Tracked) {
					auto p = touchjoint.position;
					ofDrawEllipse(toScreenPoint(mapper_.mapCameraToDepthSpace(p.x, p.y, p.z)), 2e+2 * touchjoint.force, 2e+2 * touchjoint.force);
				}
			}


		}
		// draw floor
		ofSetColor(ofColor::red);
		/*
		*/
		for (int i = 0; i < 10.; i++) {
			auto p1 = floorTrickBlock::pointOnFloor(ofVec3f(5., 0., float(i)), floorPlane);
			auto p2 = floorTrickBlock::pointOnFloor(ofVec3f(-5., 0., float(i)), floorPlane);
			auto p3 = toScreenPoint(mapper_.mapCameraToDepthSpace(p1.x, p1.y, p1.z));
			auto p4 = toScreenPoint(mapper_.mapCameraToDepthSpace(p2.x, p2.y, p2.z));
			ofDrawLine(p3, p4);
		}
	}
	body_stream_.draw();
	}

	// center of gravity
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
//--------------------------------------------------------------
void ofApp::exit()
{
	color_.close();
	depth_.close();
	ir_.close();
	body_stream_.close();
	device_->exit();
	delete device_;
	device_ = NULL;
}

