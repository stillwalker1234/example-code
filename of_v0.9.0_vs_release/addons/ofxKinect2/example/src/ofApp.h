#pragma once

#include "ofMain.h"
#include "of3dPrimitives.h"
#include "ofBodyWithForce.h"
#include "ofxKinect2.h"
#include "ofxOsc.h"

class ofApp : public ofBaseApp{

public:
	ofApp() : ofBaseApp(), count(1), limit(500), cirkleCenter(ofVec3f(-0.0712695, -0.685097, 3.02139)), cirkleRadius(2.34493) {}
	void setup();
	void update();
	void printJointToFloorDistance(JointType joint, ofxKinect2::Body& body);
	ofPoint toScreenPoint(ofVec2f point);
	void draw();

	void keyPressed(int key);
	void keyReleased(int key);
	void mouseMoved(int x, int y );
	void mouseDragged(int x, int y, int button);
	void mousePressed(int x, int y, int button);
	void mouseReleased(int x, int y, int button);
	void windowResized(int w, int h);
	void dragEvent(ofDragInfo dragInfo);
	void gotMessage(ofMessage msg);

	void exit();

	ofxKinect2::Device* device_;

	ofxKinect2::IrStream ir_;
	ofxKinect2::ColorStream color_;
	ofxKinect2::DepthStream depth_;
	ofxKinect2::BodyStream body_stream_;
	ofxKinect2::Mapper mapper_;
private:
	void setFloorPlane(ofVec3f A, ofVec3f B, ofVec3f C);
	int count;
	const int limit;
	ofPlanePrimitive plane;
	bool doDraw = true;
	ofxOscSender sender;
	ofxOscReceiver receiver;
	bool hasBodyTrack = false;
	vector<BodyWithForce> bodies;
	ofVec4f floorPlane;

	void setCirkleCenter(ofVec3f& cog) {
		cirkleCenter = floorTrickBlock::pointOnFloor(cog, floorPlane);
	}

	void setCirkleRadius(ofVec3f& cog) {
		cirkleRadius = (cirkleCenter - floorTrickBlock::pointOnFloor(cog, floorPlane)).length();
	}

	ofVec3f cirkleCenter;
	float cirkleRadius;

	void sendMes(floorTrickBlock::forceData& p, string addrs);
};
