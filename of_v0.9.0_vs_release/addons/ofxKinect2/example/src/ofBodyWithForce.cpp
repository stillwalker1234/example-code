#include "ofBodyWithForce.h"

void BodyWithForce::update(ofxKinect2::Body& body)
{

	if (body.isTracked())
	{
		// printJointToFloorDistance(JointType_FootLeft, bodies[i]);

		auto kernel = forceOnGroundBlock::getKernel(body);

		for (int j = 0; j < body.getNumJoints(); j++) {
			kernel(j);
		}

		tie(cog, touchJoints, g_force, v_force, sum_force) = forceOnGroundBlock::pop();

	}
}




