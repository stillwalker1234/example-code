#include "ofMain.h"

#include "sndfile.hh"

#ifndef waveHandler
#define waveHandler
#pragma once

class ofxWaveHandler {
	public :
		ofxWaveHandler(int minimumSec=1, int width=0, int height=0, int overviewWidth=0, int overviewHeight = 0);
		~ofxWaveHandler() {free(recBuffer);};

		void addSamples(float* input, int numSamples);
        float getSample(int startSmpl, int channel);
		int clearBuffer(); 
		void drawWaveBuffer(float xPos=0, float yPos=0);
        void drawOverviewBuffer(float xPos=0, float yPos=0);
		void drawWaveMesh(float xPos=0, float yPos=0);
		void updateWaveBuffer(unsigned int startSmpl=0, int length=0, float width = 0, float height = 0);
        void updateOverviewBuffer();
		void updateWaveMesh(int detail=0, unsigned int startSmpl=0, int length=0);
		int getBufferLengthSmpls();
        float getBufferLengthSmplsf();
		float getBufferLengthSec();
		int loadBuffer(string fileName);
		int Channels() const { return channels; }
		void Channels(int val) { channels = val; }
		ofFbo			waveForm, overView;
		ofMesh			waveMesh;
		ofEasyCam		cam;

	private:
		float*			recBuffer;
		unsigned int	recBufferMin;
		int				recPointer;
		bool			isBlocked;

		int				waveFormWidth;
		int				waveFormHeight;
        int             overviewWidth;
        int             overviewHeight;
		int				channels = 1;
		int				sampleRate;
};

#endif