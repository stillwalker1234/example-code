﻿import numpy as np
import matplotlib.pyplot as pl
from scipy.fftpack import dct # Descrite Cosine Transform
import scipy.io.wavfile as wav
import scipy.ndimage

class mfcc:
    def do_mfcc(_self, input, samplerate=44100, window_lenght=512*10, window_steps=2, n_filters=128, celps=7, n_fft=512*5):
        # set vars and create triangular filters
        input = np.array(input, dtype=float)
        _self.samplerate = samplerate
        _self.n_fft = n_fft
        _self.create_filters(n_filters)
        _self.harm_size = 5;
        # !! TODO: this should be set automaticly
        _self.th_harm = 15.0

        # slice input into windowed frames and perform time -> freq -> power transform
        frames = _self.get_frames(input, window_lenght, window_steps)
        frames_fq_domain =  np.fft.rfft(frames, (n_fft*2)-1)
        frames_power = np.apply_along_axis( _self.powerFromComplx, axis=1, arr=frames_fq_domain)

        # create mel freqency spectrum coefficients (mfsc) and mel freqency ceptral coefficients (mfcc)
        mfsc = _self.melfs(frames_power, n_filters)
        mfsc = np.log(mfsc)
        ceptrals = dct(mfsc, axis=1)[:,:celps]

        # fundamental and partials
        harm = _self.get_harm(mfsc)
        return mfsc, np.where(harm[:,:1] == _self.n_fft, 0, harm[:,:1]), ceptrals

    # finds the harm_size largest peaks for each frame and sorts them acording to bin number
    # could use a Hidden Markov Mordel to smooth values even more
    def get_harm(_self, _in):
        def get_row(_row):
            tmp = [0]
            ret = [0]
            tmp[0] = np.zeros(_self.harm_size) + _self.th_harm
            ret[0] = np.zeros(_self.harm_size) + _self.n_fft 
            state = 0 # 0: below threadshold, 1: rising, 2: falling
            trigger_value = 0.0
            def apply(k,i):
                tmp[0] = np.concatenate( [ tmp[0][1:k+1], [_row[i]], tmp[0][k+1:_self.harm_size] ] )
                ret[0] = np.concatenate( [ ret[0][1:k+1], [i], ret[0][k+1:_self.harm_size] ] )
            for i in range(np.size(_row)):
                if state == 1:
                    if _row[i] <= _row[i-1]:
                        # find where to insert
                        for k in range(_self.harm_size):                    
                            if (k == (_self.harm_size-1)) or (_row[i-1] < tmp[0][k+1]):
                                apply(k,i-1)
                                break
                        state = 2
                elif state >= 2:
                    if _row[i] < trigger_value:
                        state = 0
                    else:
                        state += 1
                elif state == 0:
                    if _row[i] > tmp[0][0]:
                        trigger_value = _row[i]
                        state = 1
            return np.sort(ret[0])
        return np.apply_along_axis(get_row, axis=1, arr=_in)

    def get_frames(_self, signal, window_lenght, window_steps):
        _retVal = []

        # calc window vector
        cos_window = np.sin(np.array(range(window_lenght))*(np.pi/window_lenght))

        # go over each window in the signal and apply window
        for i in range(0, np.size(signal)-window_lenght/window_steps, window_lenght/window_steps):
            if (i+window_lenght) > np.size(signal):
                _retVal.append( np.append( signal[i:i+window_lenght]
                                         , [0]*((i+window_lenght) - np.size(signal)))
                              * cos_window )
            else:
                _retVal.append( signal[i:i+window_lenght]*cos_window)
        return _retVal 

    def melfs(_self, input, n_filters):
        # coefficients for frames 
        tmp = np.dot(input, _self.filters.T)

        # replace 0 with smallest possible positive float, so log wont complain
        tmp = np.where(tmp == 0,np.finfo(float).eps,tmp)
        return tmp

    def create_filters(_self, n_filters, low_fq=40.0, high_fq=7000.0):
        # allocate space
        _self.filters = np.zeros((n_filters-2, _self.n_fft), dtype=float)

        # find center freqencies and corresponding fft_bins
        fq_s = (np.array(range(n_filters), dtype=float) / (n_filters-1))
        fq_s = (fq_s*(_self.fq_to_mel(high_fq)-_self.fq_to_mel(low_fq))) + float(_self.fq_to_mel(low_fq))
        _self.fq_s = _self.mel_to_feq(fq_s)
        fq_bins = _self.fq_to_bin(fq_s)

        # create filters
        for i in range(1,n_filters-2):
            denom_1 = fq_bins[i] - fq_bins[i-1] # distance from 0 to max
            denom_2 = fq_bins[i+1] - fq_bins[i] # distance from max (1) to 0
            _self.filters[i-1,:] = ( np.concatenate(
                                     [  np.array([0] * fq_bins[i-1], dtype=float) 
                                     , (np.array(range(0,denom_1), dtype=float)/denom_1)
                                     , (np.array(range(denom_2,0,-1), dtype=float)/denom_2)
                                     ,  np.array([0] * (_self.n_fft - fq_bins[i+1]), dtype=float)]) )

    def get_fq_s(_self): return _self.fq_s

    def fq_to_mel(_self, _in):
        return 1125*np.log(1+_in/700)

    def mel_to_feq(_self, _in):
        return 700*(np.exp(_in/1125)-1)

    def powerFromComplx(_self, _in):
        return 1.0/_self.n_fft * np.square( np.abs(_in))

    def bin_to_fq(_self, i):
        return (i/_self.n_fft) * _self.samplerate

    def fq_to_bin(_self, fq):
        return np.array((fq/_self.samplerate)*(_self.n_fft+1), dtype=int)
 
if __name__ == '__main__':
    x = mfcc()
    samplerate, data = wav.read('mfcc.wav', 'r')
    mfsc, fundamental, ceptrals = x.do_mfcc(data, samplerate=samplerate)

    # display result
    fq = np.floor(x.get_fq_s()[1:-1])
    fig, ax = pl.subplots(ncols=2, figsize=(14,6))
    
    ax[0].set_ylim(0,np.size(mfsc, axis=1)/2)
    ax[0].set_yticklabels(fq[ax[0].get_yticks().tolist()])
    ax[0].imshow(mfsc.T, interpolation='none')
    ax[0].set_aspect(5)
    
    ax[1].set_ylim(0,20)
    ax[1].set_yticklabels(fq[ax[1].get_yticks().tolist()])
    ax[1].plot(fundamental)
    
    pl.show()