function visualizeBoundaryLinear(X, y, model1,model2,model3)
%VISUALIZEBOUNDARYLINEAR plots a linear decision boundary learned by the
%SVM
%   VISUALIZEBOUNDARYLINEAR(X, y, model) plots a linear decision boundary 
%   learned by the SVM and overlays the data on it

w2 = model2.w;
b2 = model2.b;
w3 = model3.w;
b3 = model3.b;
w1 = model1.w;
b1 = model1.b;
xp = linspace(min(X(:,1)), max(X(:,1)), 100);
yp1 = - (w1(1)*xp + b1)/w1(2);
yp2 = - (w2(1)*xp + b2)/w2(2);
yp3 = - (w3(1)*xp + b3)/w3(2);
plotData(X, y);
hold on;
plot(xp, yp1, '-b1'); 
plot(xp, yp2, '-b2'); 
plot(xp, yp3, '-b3'); 
hold off

end
