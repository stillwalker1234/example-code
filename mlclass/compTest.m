N = 200;
w = (2*pi)/N;
o = 0;

y = zeros(N,1);
y(1) = e^(i*o);

for n=2:N
y(n) = y(n-1) * e^(i*w);
end

plot(real(y), imag(y));