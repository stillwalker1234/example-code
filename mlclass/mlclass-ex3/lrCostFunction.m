function [J, grad] = lrCostFunction(theta, X, y, lambda)
%LRCOSTFUNCTION Compute cost and gradient for logistic regression with 
%regularization
%   J = LRCOSTFUNCTION(theta, X, y, lambda) computes the cost of using
%   theta as the parameter for regularized logistic regression and the
%   gradient of the cost w.r.t. to the parameters. 

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly 
[J, grad] = costFunction(theta, X, y);

helper = ones(size(theta));
helper(1) = 0;
J += (lambda/(2*m))*(helper'*(theta.^2));
grad += (helper .*((lambda/m)*theta));
% =============================================================

grad = grad(:);

end
